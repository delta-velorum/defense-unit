const {ShardingManager} = require('discord.js');
const {debug, discord, lists} = require('../config');
const axios = require('axios');
const manager = new ShardingManager('./src/shard.js', {token: debug ? discord.test : discord.live, respawn: true}); // Since the bot is over 2k servers, it cannot login as bot, but has to be sharded

const DBL = require("dblapi.js");
const dbl = new DBL(lists.topgg);

manager.spawn('auto', 5500, 120000).catch(e => console.log(`[FATAL] Sharding Manager error: ${e.message}`));
manager.on('shardCreate', shard => {
    console.log(`[SHARDING_MANAGER] Attempting to launch shard #${shard.id}.`);
});

setInterval(async () => {
    let guilds = await manager.fetchClientValues('guilds.cache.size');


    return dbl.postStats(guilds.reduce((x, y) => x + y, 0));
}, 1_800_000);

setInterval(async () => {
    let guilds = await manager.fetchClientValues('guilds.cache.size');

    await axios.post('http://ark.delta.test/api/stats?token=HANr6or1HbUHRLQedzveYCchbTnkpGhWLLkF2HaFyzrMXIMeyhgcrbWEhMzEkGna', {
        data: [
            {
                key: 'guild',
                value: guilds.reduce((x, y) => x + y, 0)
            }
        ],
    })
}, 28_800_000);