module.exports = {
    is: async function (ownerID, knex) {
        let user = await knex.raw('select users.is_premium, (select users.id from `users` inner join `patreon_users` on `users`.`id` = `patreon_users`.`user_id` inner join `patreon_subscriptions` on `patreon_users`.`id` = `patreon_subscriptions`.`patreon_user_id` where patreon_subscriptions.tier_title in("Supporter", "Premium", "Defense Unit - Nitrado") and users.id = ?) as is_active_patron from users where users.id = ? limit 1', [
            [ownerID],
            ownerID
        ]);

        user = user[0][0];

        if (!user) {
            return false;
        }

        return !!user.is_premium || !!user.is_active_patron;
    }
}