/**
 * shard.js - The main instance of the bot
 */

const {Client, MessageEmbed} = require('discord.js'); // load discord.js, wrapper for the discord api
const {database, discord, debug, prefix, developers} = require('../config'); // load the configuration
const CommandHandler = require('./util/CommandHandler'); // load the command handler
const ServerUpdater = require('./support/Automated/ServerUpdater'); // load the command handler
const fs = require('fs');
const Premium = require('./helpers/Premium');
const Language = require('./util/Language');
const moment = require('moment');
const knex = require('knex')({
    client: 'mysql',
    connection: database
}); // Knex is our query builder, and will talk to MySQL
const {attachPaginate} = require('knex-paginate');
attachPaginate();

const client = new Client(); // new client (bot)

client.on('ready', async () => {
    console.log(`[SHARD #${client.shard.ids[0]}] Launch successful [${client.user.tag}], loading commands...`);

    // When the bot has logged into the API, we will load all commands and languages.
    client.commands = new CommandHandler();
    client.config = {
        prefix: prefix,
        developers: developers,
        debug: debug
    };
    await client.commands.load();
    await client.commands.loadLanguages();

    client.baseDir = __dirname;
    client.knex = knex; // client will be exported to almost any file so if you want to set global variables, do it via client

    let presences = [
        {
            name: 'Invite me via ,info',
            type: 'STREAMING',
            url: 'https://www.twitch.tv/Michel3951'
        },
        {
            name: 'servers.delta-velorum.com',
            type: 'STREAMING',
            url: 'https://www.twitch.tv/Michel3951'
        },
        {
            name: 'patreon.com/deltavelorum',
            type: 'STREAMING',
            url: 'https://www.twitch.tv/Michel3951'
        },
        {
            name: 'Dashboard: ark.delta-velorum.com',
            type: 'STREAMING',
            url: 'https://www.twitch.tv/Michel3951'
        }
    ];


    await client.user.setPresence({
        activity: presences[Math.floor(Math.random() * presences.length)]
    });

    setInterval(async () => {
        await client.user.setPresence({
            activity: presences[Math.floor(Math.random() * presences.length)]
        });
    }, 15 * 60_000);

    setInterval(async () => {
        try {
            let entries = await knex('reminders')
                .where({
                    was_notified: false
                })
                .where(
                    'run_at', '<=', moment().format('YYYY-MM-DD HH:mm:ss')
                );

            if (entries.length > 0) {
                let ids = [];
                let failed = []
                //console.log('[TIMER] Running ' + entries.length + ' timers.')
                for (const timer of entries) {
                    let channel = await client.channels.cache.get(timer.channel_id);

//		try {
//              	channel = await client.channels.fetch(timer.channel_id);
//		} catch (e) {
//			console.log('[TIMER] ERROR: ' + e.message + ' ID:' + timer.id);
//			continue;
//		}
//

                    if (!channel) {
                        console.log('[TIMER] No channel for ' + timer.id);
                        continue;
                    }

                    if (!channel.permissionsFor(client.user.id).has('SEND_MESSAGES')) {
                        console.log('[TIMER] Channel found but no permissions for ' + timer.id)
                        ids.push(timer.id)
                        continue;
                    }

                    ids.push(timer.id);
                    console.log('[TIMER] Sending message in ' + channel.name + ' timer ' + timer.id);
                    channel.send(`REMINDER: <@${timer.user_id}> don't forget to ${timer.message}`);
                }

                console.log('[TIMER] Updating timers: ', ids);
                await knex('reminders')
                    .whereIn('id', ids)
                    .update({
                        was_notified: true
                    });
            }
        } catch (e) {
            console.error(e)
        }

        new ServerUpdater(knex, client);
    }, debug ? 2000 : 10_000);
});

client.on('message', async message => {
    if (message.author.bot) {
        return; // if the message author is a bot, we don't need it either.
    }

    if (message.guild && !message.channel.permissionsFor(client.user.id).has('SEND_MESSAGES')) {
        return;
    }

    if (message.guild && !message.channel.permissionsFor(client.user.id).has('EMBED_LINKS')) {
        return message.reply('I do not have permission to use embeds, please ask a server admin to enable this.');
    }

    let settings = {}; // settings are for guilds, dm's will always return null, make sure to check this!

    if (message.guild) {
        // Create the guild if it was not registered already
        await knex.raw('INSERT IGNORE INTO guilds (id) VALUES (?)', [message.guild.id]);
        // retrieve the settings from the database
        settings = await knex('guilds').where({id: message.guild.id}).first('*');
        settings.command_channels = await knex('channels').where({type: 'command', guild_id: message.guild.id});
        settings.command_channels = settings.command_channels.map(x => x.channel_id);
        settings.disabled_commands = await knex('disabled_commands').where({guild_id: message.guild.id});
        settings.disabled_commands = settings.disabled_commands.map(x => x.command);
        settings.premium = await Premium.is(message.guild.ownerID, knex);
    } else {
        settings.premium = await Premium.is(message.author.id, knex);
        settings.prefix = prefix;
        settings.language = 'en';
        settings.command_channels = [];
        settings.disabled_commands = [];
    }

    // if (settings.command_channels.length > 0 && !settings.command_channels.includes(message.guild.id)) {
    //     console.log(false)
    //     return;
    // }

    if (!message.content.startsWith(settings.prefix)) {
        return;
    }

    let args = message.content.split(' ').slice(1).filter(x => x); // this will be the arguments that were passed.
    let command = message.content.split(' ')[0].slice(settings.prefix.length).toLowerCase(); // and this is the command that was executed
    command = client.commands.get(command);

    if (settings.disabled_commands.includes('command')) {
        return message.reply('This command has been disabled by a server administrator.');
    }

    if (!command) return; // if we don't have a command with this name, we do nothing
    if (command.guild && !message.guild) {
        return message.reply('This command can only be used in servers.'); // self explanatory
    }
    if (command.developer && !developers.map(x => x.id).includes(message.author.id)) {
        return message.reply('This command can only be used by the developers.'); // ^^^
    }

    let language;

    if (settings.language !== 'en') {
        language = new Language(settings.language);
    } else {
        language = new Language('en');
    }

    let user = await knex('users').where({id: message.author.id}).first();

    if (!user) {
        await knex.raw('INSERT IGNORE INTO users (id, name) values (?, ?)', [message.author.id, message.author.tag]);
    } else if (user.is_blacklisted) {
        return message.reply('You are blacklisted by one of the developers, reason: ' + user.notes)
    }


    if (!language) {
        language = new Language('en');
    }


    if (command.premium && !settings.premium) {
        return message.reply(language.get('command_is_premium'))
    }

    console.log(`[COMMAND_HANDLER] ${message.author.tag} ran the command ${command.name} in ${message.channel.id}. Arguments: [${args.map(x => `"${x}"`).join(', ')}]`);
    try {
        return command.execute(client, message, args, settings, language); // run the command!
    } catch (e) {
        fs.appendFileSync(__dirname + `/../errors/defense-unit_log_${moment().format('YYYY_MM_DD')}.txt`, `[${e.fileName}:${e.lineNumber}:${e.columnNumber}]${e.name}: ${e.message}\n${e.stack}+\r\n`);
        return message.reply(
            new MessageEmbed()
                .setColor('RED')
                .setDescription('An error occurred during the execution of the command. Please report this to the developers.')
                .addField('Error', '```' + e.message + '```')
                .addField('Message', message.content.substr(0, 1024))
        )
    }
});

process.on('unhandledRejection', (reason, promise) => {
    console.log(`[unhandledRejection] ${reason.stack || reason}\r\n\n`)
    fs.appendFileSync(__dirname + `/../errors/defense-unit_log_${moment().format('YYYY_MM_DD')}.txt`, `[unhandledRejection] ${reason.stack || reason}\r\n\n`);
});

process.on('uncaughtException', (e) => {
    console.log(`[${e.fileName}:${e.lineNumber}:${e.columnNumber}]${e.name}: ${e.message}\n${e.stack}+\r\n`);
    fs.appendFileSync(__dirname + `/../errors/defense-unit_log_${moment().format('YYYY_MM_DD')}.txt`, `[${e.fileName}:${e.lineNumber}:${e.columnNumber}]${e.name}: ${e.message}\n${e.stack}+\r\n`);
});

client.login(debug ? discord.test : discord.live).catch(e => console.log(`[FATAL] Could not login to API: ${e.message}`));
