const DefaultFoodValue = require('./DefaultFoodValue');
const SpecialFoodValue = require('./SpecialFoodValue');
const {tamingFoodData} = require('../../json/taming_food');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

/**
 * Represents the food that is eaten by a creature
 * @property {Array} eats - An array of strings containing the names of edibles that the creature can eat
 * @property {String} favoriteKibble - The favorite kibble of the creature
 * @property {Array} specialFoodValues - An array of object containing information about the food that the creature eats
 */

class CreatureFood {
    eats = [];
    favoriteKibble = [];
    specialFoodValues = [];


    /**
     * Initialize a new instance
     * @param {Creature} creature - The creature
     */
    constructor(creature) {
        if (!tamingFoodData.hasOwnProperty(creature.name)) {
            throw new TypeError('Creature has no taming food data.');
        }

        const data = tamingFoodData[creature.name];

        this.favoriteKibble = data.favoriteKibble;
        if (data.eats === null) {
            return;
        }

        for (let i = 0; i < data.eats.length; i++) {
            let food = data.eats[i];

            this.eats.push({
                name: food,
            });
        }

        Object.entries(data.specialFoodValues).forEach((object) => {
            let displayName = (object[0] === 'Kibble') ? `Kibble (${data.favoriteKibble})` : object[0];

            this.specialFoodValues.push(new SpecialFoodValue(object, displayName))
        });

        this.loadDefaults(tamingFoodData, data.favoriteKibble);
    }

    /**
     * Load the default food values for the creature
     * @param {Array} data - taming_food.json
     * @param {String} favoriteKibble - The creatures favorite kibble
     */
    loadDefaults(data, favoriteKibble) {
        data = data['default'];
        Object.entries(data.specialFoodValues).forEach((object) => {
            if (!this.eats.map(x => x.name).includes(object[0])) {
                return; // if the food isn't eaten by the creature, we won't need it.
            }

            let array = this.specialFoodValues.map(x => x.name);

            if (array.includes(object[0])) {
                return;
                // if this is already in the array it means that it is a special value,
                // and does not need to be overwritten by the default
            }

            let displayName = (object[0] === 'Kibble') ? `Kibble (${favoriteKibble} Egg)` : object[0];
            this.specialFoodValues.push(new DefaultFoodValue(object, displayName))
        });
    }

    /**
     *
     * @param {Creature} creature - The creature
     * @param {Number} level - The taming level
     * @param {String} foodName - The name of the food
     * @param {Number} tamingSpeedMultiplier - GameServer taming speed multiplier
     * @returns {Number} - The amount of food needed to tame the creature
     */
    static foodAmountNeeded(creature, level, foodName, tamingSpeedMultiplier = 1) {
        if (creature === null) {
            return 0;
        }

        tamingSpeedMultiplier = tamingSpeedMultiplier * 2;

        let affinityNeeded = creature.taming.affinityNeeded0 + creature.taming.affinityIncreasePL * level;
        let specialFoodKeys = creature.food.specialFoodValues ? creature.food.specialFoodValues.map(value => value.name) : []; // map the special food value names
        let isSpecialFood = specialFoodKeys.includes(foodName); // check if the food has a special value

        if (!isSpecialFood) return 0;

        let foodAffinity = creature.food.specialFoodValues.filter(x => x.name === foodName)[0].affinity;

        if (creature.taming.nonViolent) foodAffinity *= creature.taming.wakeAffinityMultiplier;

        foodAffinity *= tamingSpeedMultiplier * 2;

        if (foodAffinity > 0) {
            let quantity = 1;
            return Math.ceil(affinityNeeded / (foodAffinity * quantity));
        }

        return 0;
    }

    /**
     * Calculates the time needed to tame the creature with this food
     * @param {Creature} creature - The creature
     * @param {Number} foodQuantity - The amount of food
     * @param {DefaultFoodValue|SpecialFoodValue} food - The food
     * @param {Number} tamingFoodRateMultiplier - The server's food rate multiplier
     * @returns {moment.Duration} - The duration in seconds
     */
    static tamingDuration(creature, foodQuantity, food, tamingFoodRateMultiplier = 1) {
        let seconds = 0;

        let foodValue = food.food;

        if (creature.taming.nonViolent) foodValue = foodValue * creature.taming.wakeFoodDepletionMultiplier;

        if (creature.name === 'Mantis') {
            seconds = foodQuantity * 180; // mantis eats every 3 minutes, regardless of level
        } else {
            seconds = Math.ceil(foodQuantity * foodValue / (creature.taming.foodConsumptionBase * creature.taming.foodConsumptionMultiplier * tamingFoodRateMultiplier));
        }

        return moment.duration(seconds, 'seconds');
    }
};

module.exports = CreatureFood;