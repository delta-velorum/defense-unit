module.exports = class DefaultFoodValue {
    name = null;
    food = 0;
    affinity = 0;
    quantity = 0;
    displayName = null;
    special = false;

    constructor(object, displayName) {
        this.displayName = displayName;
        this.name = object[0];
        this.food = object[1].f;
        this.affinity = object[1].a;
    }
};

