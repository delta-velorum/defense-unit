const TamingStat = require('./TamingStat');

module.exports = class FullStatsRaw {
    static health = 0;
    static stamina = 1;
    static torpidity = 2;
    static oxygen = 3;
    static food = 4;
    static water = 5;
    static temperature = 6;
    static weight = 7;
    static meleeDamageMultiplier = 8;
    static speedMultiplier = 9;
    static temperatureFortitude = 10;
    static craftingSpeedMultiplier = 11;
    static stats = [
        'health',
        'stamina',
        'torpidity',
        'oxygen',
        'food',
        'water',
        'temperature',
        'weight',
        'meleeDamageMultiplier',
        'speedMultiplier',
        'temperatureFortitude',
        'craftingSpeedMultiplier'
    ];
    static absVersion = "13.1";

    // An array of all the stats mentioned above
    stats = [];

    constructor(fullStatsRaw) {
        // loop over all stats
        for (let stat = 0; stat < FullStatsRaw.stats.length; stat++) {
            let array = [0, 0, 0, 0, 0]; // set the 5 default stats, they will be re-assigned later (if they exist)

            for (let i = 0; i < 5; i++) { // loop over the stats values
                const current = fullStatsRaw[stat];

                if (current !== null && current.length > i) { // if the value exists in the stat, we update it in the array
                    array[i] = current[i];
                }
            }

            this.stats.push(new TamingStat(array, FullStatsRaw.stats[stat]));
        }
    }

    get(index) {
        return this.stats[index];
    }
};
