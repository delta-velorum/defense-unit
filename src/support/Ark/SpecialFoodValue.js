module.exports = class SpecialFoodValue {
    name = null;
    food = 0;
    affinity = 0;
    quantity = 0;
    displayName = null;
    special = true;

    constructor(object, displayName) {
        this.name = object[0];
        this.displayName = displayName;
        this.food = object[1].f;
        this.affinity = object[1].a;
    }
};
