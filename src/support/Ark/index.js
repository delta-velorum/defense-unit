module.exports = {
    Creature: require('./Creature'),
    CreatureFood: require('./CreatureFood'),
    CreatureTaming: require('./CreatureTaming'),
    TamingStat: require('./TamingStat'),
    SpecialFoodValue: require('./SpecialFoodValue'),
    DefaultFoodValue: require('./DefaultFoodValue'),
    FullStatsRaw: require('./FullStatsRaw'),
};