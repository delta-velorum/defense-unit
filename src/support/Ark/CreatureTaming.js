const FullStatsRaw = require('./FullStatsRaw');

/**
 * @property {Boolean} nonViolent - Is the creature tamed non violent
 * @property {Boolean} violent - Will the creature fight back
 * @property {Number} tamingIneffectiveness - ????
 * @property {Number} affinityNeeded0 - Affinity needed at level 0
 * @property {Number} affinityIncreasePL - Extra affinity needed per level
 * @property {Number} torporDepletionPS0 - Torpor depletion per second
 * @property {Number} foodConsumptionBase - The base food consumption
 * @property {Number} wakeAffinityMultiplier - The wake-up affinity multiplier
 * @property {Number} wakeFoodDepletionMultiplier -The food depletion multiplier
 */
class CreatureTaming {
    nonViolent = false;
    violent = true;
    tamingIneffectiveness = 0;
    affinityNeeded0 = 0;
    affinityIncreasePL = 0;
    torporDepletionPS0 = 0;
    foodConsumptionBase = 0;
    foodConsumptionMultiplier = 0;
    wakeAffinityMultiplier = 0;
    wakeFoodDepletionMultiplier = 0;

    constructor(json) {
        this.nonViolent = json.nonViolent;
        this.violent = json.violent;
        this.tamingIneffectiveness = json.tamingIneffectiveness;
        this.affinityNeeded0 = json.affinityNeeded0;
        this.affinityIncreasePL = json.affinityIncreasePL;
        this.torporDepletionPS0 = json.torporDepletionPS0;
        this.foodConsumptionBase = json.foodConsumptionBase;
        this.foodConsumptionMultiplier = json.foodConsumptionMult;
        this.wakeFoodDepletionMultiplier = json.wakeAffinityMult;
        this.wakeAffinityMultiplier = json.wakeFoodDeplMult;
    }

    /**
     *
     * @param {Creature} creature
     * @param {CreatureFood} food
     * @param {Number} level
     * @param {Number} quantity
     * @param tamingSpeedMultiplier
     * @returns {{effectiveness: <Number>, levels: <Number>}}
     */
    static effectiveness(creature, food, level = 150, quantity = 1, tamingSpeedMultiplier = 1) {
        // food by Affinity
        let fa = 0;
        let special = food.special;
        let affinityNeeded = creature.taming.affinityNeeded0 + creature.taming.affinityIncreasePL * level;
        let requiredFood = 0;
        let foodByAffinity = 0;
        let levels = 0;
        let effectiveness = 0;

        if (special || food) {
            let value = 0;
            let affinity = 0;

            if (special) {
                affinity = food.affinity;
                value = food.food;
            } else {
                affinity = food.affinity;
                value = food.food;
            }

            affinity *= special ? quantity : 1;

            if (creature.taming.nonViolent) {
                affinity *= creature.taming.wakeAffinityMultiplier;
                value = value * creature.taming.wakeAffinityMultiplier;
            }

            affinity *= tamingSpeedMultiplier * 2;

            if (affinity > 0 && value > 0) {
                let seconds = 0;
                let totalFoodRequired = Math.ceil(affinityNeeded / affinity);

                if (totalFoodRequired > quantity) {
                    requiredFood = totalFoodRequired;
                }

                if (creature.name === 'Mantis') {
                    seconds = totalFoodRequired * 180;
                } else {
                    seconds = Math.ceil(totalFoodRequired * value / (creature.taming.foodConsumptionBase * creature.taming.foodConsumptionMultiplier));
                    affinityNeeded -= totalFoodRequired * affinity;
                }

                foodByAffinity += totalFoodRequired / affinity;

                seconds += seconds;
            }

            let te = 1 / (1 + creature.taming.tamingIneffectiveness * foodByAffinity);
            if (te < 0) {
                te = 0;
            }

            effectiveness = te;
            levels = Math.floor(level * te / 2);

        }

        return {effectiveness: (effectiveness * 100), levels: levels};
    }

    static torporDepletionPerSecond(torporDepletionPS0, level = 150) {

        // "borrowed" from crumple corn

        if (torporDepletionPS0 > 0) {
            return torporDepletionPS0 + Math.pow(level - 1, 0.800403041) / (22.39671632 / torporDepletionPS0);
        }

        return 0;
    }

    /**
     * Calculate the time in seconds before a creature wakes up
     * @param level
     * @param fullStatsRaw
     * @returns {number}
     */
    getWakeUpTime(level = 150, fullStatsRaw) {
        let depletionPerSecond = CreatureTaming.torporDepletionPerSecond(this.torporDepletionPS0, level);
        let totalTorpor = this.getTotalTorpor(level, fullStatsRaw);

        return totalTorpor / depletionPerSecond; // the time it takes in seconds
    }

    /**
     * Calculate the total torpor for this creature
     * @param level
     * @param fullStatsRaw
     * @returns {number}
     */
    getTotalTorpor(level = 150, fullStatsRaw) {
        let baseTorpor = fullStatsRaw.stats[FullStatsRaw.torpidity].baseValue;
        let torporIncrementPerLevel = fullStatsRaw.stats[FullStatsRaw.torpidity].incrementPerWildLevel;

        return baseTorpor + ((level - 1) * (baseTorpor * torporIncrementPerLevel));
    }

    /**
     *
     */
};

module.exports = CreatureTaming;