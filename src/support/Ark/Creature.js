const FullStatsRaw = require('./FullStatsRaw');
const CreatureTaming = require('./CreatureTaming');
const CreatureFood = require('./CreatureFood');
const {species} = require('../../json/creature_stats');

/**
 * Represents a creature
 * @property {String} name - The creature name
 * @property {String} blueprintPath - The path to the blueprint
 * @property {FullStatsRaw} fullStatsRaw - The stats of this creature, mapped by numeric values
 * @property {CreatureTaming} taming - See {Taming}
 * @property {Array} immobilizedBy - An array of strings containing traps that immobilize the creature
 * @property {CreatureFood} food - See {CreatureFood}
 * @property {Array} colors - TODO: implement colors support
 * @property {Boolean} noImprintingForSpeed - Does this creature have imprint stats for speed
 * @property {Boolean} doesNotUseOxygen - Does this creature use oxygen (underwater?)
 * @property {Number} displayedStats - ????
 */

class Creature {
    name = null;
    blueprintPath = null;
    fullStatsRaw = FullStatsRaw;
    taming = CreatureTaming;
    immobilizedBy = [];
    food = CreatureFood;
    colors = [];
    tamedBaseHealthMultiplier = 0;
    noImprintingForSpeed = false;
    doesNotUseOxygen = false;
    displayedStats = 0;

    /**
     * Initialize a new creature
     * @param {Object|String} object - A JSON object of the creature, or the creature's name
     */

    constructor(object) {

        if (typeof object === 'string') {
            let creature = species.filter(x => x.name === object)[0];
            if (!creature) throw new TypeError('Creature does not have any stats');
            object = creature;
        }

        this.name = object.name;
        this.blueprintPath = object.blueprintPath;
        this.fullStatsRaw = new FullStatsRaw(object.fullStatsRaw);
        this.taming = new CreatureTaming(object.taming);
        this.immobilizedBy = object.immobilizedBy;
        this.colors = object.colors;
        this.tamedBaseHealthMultiplier = object.TamedBaseHealthMultiplier;
        this.noImprintingForSpeed = object.NoImprintingForSpeed;
        this.doesNotUseOxygen = object.doesNotUseOxygen;
        this.displayedStats = object.displayedStats;
        this.food = new CreatureFood(this);
    }
};

module.exports = Creature;