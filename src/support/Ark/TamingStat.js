module.exports = class TamingStat {
    name = null;
    baseValue = 0;
    incrementPerWildLevel = 0;
    incrementPerTamedLevel = 0;
    addWhenTamed = 0;
    affinityMultiplier = 0;

    constructor(stat, name) {
        this.name = name;
        this.baseValue = stat[0];
        this.incrementPerWildLevel = stat[1];
        this.incrementPerTamedLevel = stat[2];
        this.addWhenTamed = stat[3];
        this.affinityMultiplier = stat[4];
    }
}
