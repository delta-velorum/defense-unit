const {MessageEmbed} = require('discord.js');

module.exports = {
    getOverview: function (server, language) {
        let embed = new MessageEmbed();

        embed.setColor('GREEN');
        embed.setTitle(server.name);
        embed.setURL(`https://battlemetrics.com/servers/ark/${server.id}`);
        embed.addField(language.get('players'), `${server.players.list.length}/${server.players.max}`, true);
        embed.addField(language.get('platform'), language.get('pc'), true);
        embed.addField(language.get('map'), server.map, true);
        if (server.day && server.day > 0) {
            embed.addField(language.get('day'), server.day, true);
        }
        embed.setThumbnail('https://www.battlemetrics.com/favicon-194x194.png');
        embed.addField(language.get('is_pve'), server.pve ? language.get('yes') : language.get('no'), true);
        embed.addField(language.get('is_official'), server.pve ? language.get('yes') : language.get('no'), true);
        embed.setFooter('Powered by battlemetrics.com', 'https://www.battlemetrics.com/favicon-194x194.png');
        return embed;
    },
    getPlayers: function (server, language) {
        let embed = new MessageEmbed();

        embed.setColor('GREEN');
        embed.setTitle(server.name);
        embed.setURL(`https://battlemetrics.com/servers/ark/${server.id}`);
        if (server.players.list.length > 0) {
            embed.setDescription('```' + server.players.list.join('\n').substr(0, 2042) + '```');
        } else {
            embed.setDescription(language.get('server_no_players'))
        }
        embed.setFooter('Powered by battlemetrics.com', 'https://www.battlemetrics.com/favicon-194x194.png');
        return embed;
    }
};