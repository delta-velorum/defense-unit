const axios = require('axios');
const {MessageEmbed, ReactionCollector} = require('discord.js');
const {getOverview, getPlayers} = require('./BattlemetricsEmbed');
const maps = require('../../json/maps.json');

class Battlemetrics {
    id = null;
    #base = 'https://api.battlemetrics.com';
    name = null;
    ip = null;
    port = null;
    players = {
        list: [],
        max: 0
    };
    status = null;
    map = null;
    official = false;
    pve = false;
    steamID = null;
    day = 0;


    constructor(id) {
        this.id = id;
        return this.fetch();
    }

    async fetch() {
        let {data} = await axios.get(this.#base + '/servers/' + this.id + '?include=player');
        const included = data.included;
        data = data.data;

        if (data.relationships.game.data.id !== 'ark') {
            throw new TypeError('server_must_run_ark');
        }

        this.players.list = included.filter(x => x.type === 'player').map(x => x.attributes.name);

        const server = data.attributes;
        const about = server.details;

        this.ip = server.ip;
        this.name = server.name;
        this.port = server.port;
        this.status = server.status;
        this.official = about.official;
        this.pve = about.pve;
        this.map = about.map;
        this.steamID = about.serverSteamId;
        this.players.max = server.maxPlayers;
        this.day = about.time;
        return this;
    }

    async save(knex, guild) {
        let exists = await knex('servers').where({guild_id: guild.id, server_id: this.id}).first();

        if (exists) return false;

        await knex('servers').insert({
            guild_id: guild.id,
            server_id: this.id,
            platform: 'pc',
            name: this.name,
        });

        return this.name
    }

    async toEmbed(message = null, language) {

        let overview = getOverview(this, language);

        if (!message) {
            return overview;
        }

        let players = getPlayers(this, language);

        let msg = await message.reply(`steam://connect/${this.ip}:${this.port}`, {
            embed: overview
        });

        const EMOJI_OVERVIEW = '📰';
        const EMOJI_PLAYERS = '📈';

        await msg.react(EMOJI_OVERVIEW);
        await msg.react(EMOJI_PLAYERS);

        let page = 0;
        const filter = (reaction, user) => (user.id === message.author.id) && [EMOJI_OVERVIEW, EMOJI_PLAYERS].includes(reaction.emoji.name);

        let collector = new ReactionCollector(msg,
            filter,
            {
                time: 6e4,
                dispose: true,
            }
        );

        collector.on('collect', (reaction, user) => {
            if (reaction.emoji.name === EMOJI_PLAYERS && page !== 1) {
                page = 1;
                msg.edit(`steam://connect/${this.ip}:${this.port}`, {
                    embed: players
                })
            } else if (reaction.emoji.name === EMOJI_OVERVIEW && page !== 0) {
                page = 0;
                msg.edit(`steam://connect/${this.ip}:${this.port}`, {
                    embed: overview
                });
            }
        });

        collector.on('end', async () => {
           if (message.guild.me.hasPermission("MANAGE_MESSAGES")) {
               await msg.reactions.removeAll();
           }
        });

        return true;
    }

    async toWatcher(guild) {
        return new MessageEmbed()
            .setColor(this.status === 'online' ? 'GREEN' : 'RED')
            .setAuthor(this.name, guild.iconURL())
            .addField('Status', this.status === 'online' ? '<:online:542765234449023026> online' : '<:offline:542765308226830354> offline')
            .addField('Players', `${this.players.current}/${this.players.max}`)
            .addField('Platform', 'PC')
            .addField('Map', maps[this.map] || this.map);
    }
}

module.exports = Battlemetrics;