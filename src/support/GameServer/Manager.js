const Battlemetrics = require('./Battlemetrics');
const Dedicated = require('./Dedicated');
const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;

/**
 * @property {Client} client
 * @property {Message} message
 * @property {Array} args
 * @property {Object} settings
 * @property {Language} language
 */
class Manager {
    client = {};
    args = [];
    message = null;
    settings = {};
    language= {};

    constructor(client, args, message, settings, language) {
        this.client = client;
        this.args = args;
        this.knex = client.knex;
        this.message = message;
        this.settings = settings;
        this.language = language;
    }

    async list() {
        if (isNaN(this.args[1]) && this.args[1] !== undefined) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), this.language.get('page_number')))
        }

        let page = this.args[1] ? Math.floor(this.args[1]) : 1;
        if (page <= 0) page = 1;

        const itemsPerPage = 10;

        let {pagination, data} = await this.knex('servers')
            .where({
                guild_id: this.message.guild.id
            })
            .paginate({
                perPage: itemsPerPage,
                currentPage: page,
                isLengthAware: true
            });

        if (data.length === 0) {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('ORANGE')
                    .setAuthor(sprintf(this.language.get('server_list_title'), this.message.guild.name), this.message.guild.iconURL())
                    .setDescription(this.language.get('no_entries'))
            )
        }

        let servers = await this.knex('servers').where({
            guild_id: this.message.guild.id
        });

        let embed = new MessageEmbed();
        embed.setTitle(sprintf(this.language.get('server_list_title'), this.message.guild.name));

        embed.setColor('GREEN');
        embed.setDescription(
            '```\n' +
            data.map(x => `#${x.server_id} - ${x.name} (${x.platform})`).join('\n').substr(0, 2040)
            + '```'
        );

        return this.message.reply(embed);
    }

    async add() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('server_add_must_specify_type'));
        }

        // if (!this.settings.premium) {
        //     let {servers} = await this.knex('servers').where({guild_id: this.message.guild.id}).count('* as servers').first();
        //
        //     if (servers >= 10) {
        //         return this.message.reply(this.language.get('server_max_limit'))
        //     }
        // }

        const platform = this.args[1].toLowerCase();

        if (['xbox', 'mobile', 'switch', 'ps4', 'pc'].includes(platform)) {
            if (isNaN(this.args[2])){
                return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'));
            }

            const server = platform === 'pc' ? await new Battlemetrics(this.args[2]) : await new Dedicated(this.args[2]);

            if (!server) {
                return this.message.reply(this.language.get('server_not_found_api'));
            }

            let result = await server.save(this.knex, this.message.guild);

            if (result) {
                return this.message.reply(sprintf(this.language.get('server_add_success'), result));
            } else {
                return this.message.reply(this.language.get('server_add_already_exists'));
            }

        } else {
            return this.message.reply(sprintf(this.language.get('server_add_unknown_platform'), this.args[1]))
        }
    }

    help() {
        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setDescription(this.language.get('server_what_is_it'))
                .addField('add', this.language.get('server_help_create'))
                .addField('remove', this.language.get('server_help_remove'))
                .addField('list', this.language.get('server_help_list'))
                .addField('ID_OR_NAME', this.language.get('server_help_view'))
                .setTitle(this.language.get('server_help_title'))
        )
    }

    async remove() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('server_remove_must_specify_id'));
        }

        let server = await this.knex('servers').where({guild_id: this.message.guild.id, server_id: this.args[1]}).first();

        if (!server) {
            return this.message.reply(this.language.get('server_remove_server_not_found'));
        }

        await this.knex('servers').where({guild_id: this.message.guild.id, server_id: this.args[1]}).delete();

        return this.message.reply(sprintf(this.language.get('server_remove_success'), server.name));
    }

    async delete() {
        await this.remove();
    }

    async link() {
        await this.add();
    }
}

module.exports = Manager;