const axios = require('axios');
const {MessageEmbed} = require('discord.js');
const platforms = require('../../json/platforms.json');
const maps = require('../../json/maps.json');

class Dedicated {
    id = null;
    #base = 'https://servers.delta-velorum.com/api/v1';
    name = null;
    ip = null;
    port = null;
    players = {
        current: 0,
        max: 0
    };
    map = null;
    platform = null;
    official = false;
    pve = false;
    day = null;
    cluster = false;


    constructor(id) {
        this.id = id;
        return this.fetch();
    }

    async fetch() {
        let {data} = await axios.get(this.#base + '/servers/' + this.id + '?include=players');
        let server = data.data;

        this.ip = server.ip;
        this.name = server.name;
        this.port = server.port;
        this.official = server.official;
        this.pve = server.pve;
        this.players.current = server.players.count;
        this.players.max = server.max_players;
        this.platform = server.platform;
        this.day = server.day_time;
        this.map = server.map;
        this.status = server.status;
        this.cluster = !!server.cluser_id;
        this.version = `${server.build_id}.${server.minor_build_id}`;
        return this;
    }

    async save(knex, guild) {
        let exists = await knex('servers').where({guild_id: guild.id, server_id: this.id}).first();

        if (exists) return false;

        await knex('servers').insert({
            guild_id: guild.id,
            server_id: this.id,
            platform: this.platform,
            name: this.name,
        });

        return this.name;
    }

    async toEmbed(message = null, language) {
        let embed = new MessageEmbed();

        embed.setColor('GREEN');
        embed.setTitle(this.name);
        embed.setURL(`https://servers.delta-velorum.com/server/${this.id}`);
        embed.addField(language.get('players'), `${this.players.current}/${this.players.max}`, true);
        embed.addField(language.get('platform'), language.get(this.platform), true);
        embed.setThumbnail('https://servers.delta-velorum.com/favicon.png');
        embed.addField(language.get('map'), this.map, true);
        embed.addField(language.get('version'), this.version, true);
        embed.addField(language.get('status'), this.status, true);
        if (this.day && this.day > 0) {
            embed.addField(language.get('day'), this.day, true);
        }
        embed.addField(language.get('is_pve'), this.pve ? language.get('yes') : language.get('no'), true);
        embed.addField(language.get('is_official'), this.pve ? language.get('yes') : language.get('no'), true);
        embed.setFooter('Powered by servers.delta-velorum.com', 'https://servers.delta-velorum.com/favicon.png');
        return message ? message.reply(embed) : embed;
    }

    async toWatcher(guild) {
        return new MessageEmbed()
            .setColor(this.status === 'online' ? 'GREEN' : 'RED')
            .setAuthor(this.name, guild.iconURL())
            .addField('Status', this.status === 'online' ? '<:online:542765234449023026> online' : '<:offline:542765308226830354> offline')
            .addField('Players', `${this.players.current}/${this.players.max}`)
            .addField('Platform', platforms[this.platform] || this.platform)
            .addField('Map', maps[this.map] || this.map);
    }
}

module.exports = Dedicated;