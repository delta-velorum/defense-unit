const moment = require('moment');
const {Battlemetrics, Dedicated} = require('../../support/GameServer');
const Language = require('../../util/Language');
const Premium = require('../../helpers/Premium');

module.exports = class ServerUpdater {
    constructor(knex, client) {
        this.knex = knex;
        this.client = client;

        this.channels = new Map();
        this.servers = new Map();
        this.language = new Language('en');
        this.shardGuilds = new Set();

        this.x = false;

        this.initialize();
    }

    async initialize() {
        const now = moment().subtract(30, 'minutes').format('YYYY-MM-DD HH:mm:ss');

        let servers = await this.knex('watched_servers')
            .where('updated_at', '<=', now)
            .orWhere({
                updated_at: null
            }).limit(5);

        if (servers.length === 0) {
            return;
        }

	let doUpdate = [];

        //console.log(`[SERVER_UPDATER] Updating ${servers.length} servers.`)

        await this.mapChannelsByGuild(servers);

        for (let server of servers) {
            if (!this.channels.has(server.guild_id)) {
                continue;
            }

            let channel = await this.client.channels.cache.get(
                this.channels.get(server.guild_id)
	    );

            if (!channel) {
                continue;
            }

            let isPremium = await Premium.is(channel.guild.ownerID, this.knex);

            if (!isPremium) {
                await this.knex('watched_servers')
                    .where({
                        server_id: server.server_id,
                        guild_id: server.guild_id,
                    }).delete();
                continue;
            }

            this.shardGuilds.add(channel.guild.id);

            if (!channel.permissionsFor(this.client.user.id).has('SEND_MESSAGES')) {
                continue;
            }

            let api = await this.fetchServer(server.server_id, server.platform);

	  if (!api) {
		continue;
		}

            if (!server.message_id) {
                await this.sendFirstMessage(channel, api);
                continue;
            }

            let message;

            try {
                message = await channel.messages.fetch(server.message_id);
            } catch (e) {
                message = null;
            }


            if (!message) {
                await this.sendFirstMessage(channel, api);
                continue;
            }

            await this.updateExistingMessage(message, api);
        }

        servers = new Set(this.servers.keys());

	console.log(Array.from(servers))

        await this.knex('watched_servers')
            .whereIn('guild_id', Array.from(this.shardGuilds))
            .whereIn('server_id', Array.from(servers))
            .update({
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
            });
    }

    async mapChannelsByGuild(watchedServers) {
        let channels = await this.knex('channels')
            .whereIn('guild_id', watchedServers.map(x => x.guild_id))
            .where({
                type: 'server'
            });

        for (const channel of channels) {
            if (!this.channels.has(channel.guild_id)) {
                this.channels.set(channel.guild_id, channel.id);
            }
        }
    }

    async sendFirstMessage(channel, server) {
        let embed = await server.toWatcher(channel.guild);

        embed.footer = null;
        embed.thumbnail = null;

        let message = await channel.send(embed);

        await this.knex('watched_servers')
            .where({
                guild_id: channel.guild.id,
                server_id: server.id,
            })
            .update({
                message_id: message.id,
            })

        return message;
    }

    async updateExistingMessage(message, server) {
        let embed = await server.toWatcher(message.guild);

        embed.footer = null;
        embed.thumbnail = null;

        return message.edit({embed: embed});
    }

    async fetchServer(server_id, platform) {
        if (this.servers.has(server_id)) {
            return this.servers.get(server_id);
        }

	try {
        let server = platform === 'pc' ? await new Battlemetrics(server_id) : await new Dedicated(server_id);
        this.servers.set(server.id, server);
	return server;
        }catch(e) {

	}

        return null;
    }
}
