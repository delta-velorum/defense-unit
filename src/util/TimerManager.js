const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');

module.exports = class TimerManager {
    constructor(client, args, message, settings, language) {
        this.client = client;
        this.args = args;
        this.knex = client.knex;
        this.message = message;
        this.settings = settings;
        this.language = language;
    }

    async list() {
        let rows = await this.knex('reminders')
            .where({user_id: this.message.author.id, was_notified: false})
            .orderBy('run_at')
            .limit(10);

        if (rows.length === 0) {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('ORANGE')
                    .setDescription(this.language.get('no_entries'))
            )
        } else {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('GREEN')
                    .setDescription(
                        rows.map(x => {
                            return `#${x.id} - ${x.message.substr(0, 50)}...`
                        })
                    )
                    .setFooter(this.language.get('timer_cancel_tip'))
            )
        }
    }

    async cancel() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('timer_cancel_id'))
        }

        let result = await this.knex('reminders')
            .where({
                user_id: this.message.author.id,
                id: this.args[1]
            })
            .delete();

        if (result === 1) {
            return this.message.reply(
                this.language.get('timer_successfully_cancelled')
            );
        } else {
            return this.message.reply(
                this.language.get('timer_not_cancelled')
            );
        }
    }
};
