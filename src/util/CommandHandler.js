const {Collection} = require('discord.js');
const fs = require('fs');

class CommandHandler {
    constructor() {
        this.commands = new Collection();
        this.aliases = new Collection();
        this.languages = new Collection();
    }

    async load() {
        // retrieve all JS files from the commands directory
        let files = fs.readdirSync(__dirname + '/../commands');
        files = files.filter(file => file.endsWith('.js'));

        console.log(`[COMMAND_HANDLER] Attempting to load ${files.length} command(s).`);

        files.forEach(file => {
            const command = require(`../commands/${file}`); // load command to cache
            this.commands.set(command.name, command); // add command to the command list
            command.aliases.forEach(alias => {
                this.aliases.set(alias, command); // if the command has an alias, set it in the aliases list
            });
        });

        console.log(`[COMMAND_HANDLER] Successfully loaded ${files.length} command(s).`);

        return this.commands;
    }

    async loadLanguages() {
        // load languages to cache, then do the same as the cmd handler
        let files = fs.readdirSync(__dirname + '/../lang');
        files = files.filter(file => file.endsWith('.json'));

        files.forEach(file => {
            const pack = require(__dirname + `/../lang/${file}`);
            this.languages.set(file.replace('.json', ''), pack);
        });

        return this.languages;
    }

    get(name) {
        if (this.commands.has(name)) return this.commands.get(name);
        if (this.aliases.has(name)) return this.aliases.get(name);
        return null;
    }

    has(name) {
        return !!(this.commands.has(name) || this.aliases.has(name))
    }

    reload(name) {
        delete require.cache[require.resolve(`../commands/${name}.js`)];

        const aliases = this.commands.get(name).aliases;

        aliases.forEach(alias => {
            this.aliases.delete(alias);
        });

        this.commands.delete(name);

        const command = require(`../commands/${name}.js`);

        this.commands.set(name, command);

        command.aliases.forEach(alias => {
           this.aliases.set(alias, command);
        });
    }
}

module.exports = CommandHandler;