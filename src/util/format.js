const moment = require('moment');

module.exports = {
    toTimeString: function (seconds, language) {
        const duration = moment.duration(seconds, 'seconds');
        const times = ['years', 'months', 'days', 'hours', 'minutes', 'seconds'];
        let string = '';
        let matches = 0;

        for (let i = 0; i < times.length; i++) {
            const time = duration[times[i]]();
            if (time !== 0) { // if the duration is 0, we dont need it
                if (time > 1) {
                    string += `${string.length > 0 ? ' ' : ''}${time} ${language.get(times[i])}`; // seconds, minutes etc
                } else {
                    string += `${string.length > 0 ? ' ' : ''}${time} ${language.get(times[i].replace(/s$/, ''))}`; // second, minute
                }

                matches++;
            }
        }

        if (matches > 1) {
            let last = string.match(/([0-9]+ [a-z]+)$/);
            if (last !== null) {
                string = string.replace(last[0], `${language.get('and')} ${last[0]}`)
            }
        }

        return string;
    },
};