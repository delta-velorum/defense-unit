const parse = require('yargs-parser');

class Arguments {
    constructor(string) {
        for (const [key, value] of Object.entries(parse(string))) {
            this[key] = value;
        }
    }

    has(key) {
        return this.hasOwnProperty(key);
    }

    get(key, fallback = undefined) {
        if (!this.has(key)) {
            return fallback;
        }

        return this[key];
    }
}

module.exports = Arguments;