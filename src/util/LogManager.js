const {MessageCollector, MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');

module.exports = class LogManager {
    constructor(client, args, message, settings, language) {
        this.client = client;
        this.args = args;
        this.knex = client.knex;
        this.message = message;
        this.settings = settings;
        this.language = language;
    }

    async add() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('log_create_must_have_content'));
        }

        const content = this.args.slice(1).join(' ');

        await this.knex.raw('INSERT IGNORE INTO users (id, name) values (?, ?)', [this.message.author.id, this.message.author.tag]);

        const row = await this.knex('logs').insert({
            user_id: this.message.author.id,
            content: content,
        });

        return this.message.reply(sprintf(this.language.get('log_created'), row[0]))
    }

    async title() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('log_title_must_specify_id'));
        }

        if (isNaN(this.args[1])) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'))
        }

        const log = await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .first();

        if (!log) {
            return await this.message.reply(sprintf(this.language.get('log_not_found_user'), this.args[1]));
        }

        if (!this.args[2]) {
            return this.message.reply(this.language.get('log_title_must_specify_content'));
        }

        const title = this.args.slice(2).join(' ');

        if (title.length > 128) {
            return this.message.reply(this.language.get('log_title_string_limit'))
        }

        await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .update({title: title});
        return this.message.reply(sprintf(this.language.get('log_title_edited_successfully'), title));
    }

    async view() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('log_title_must_specify_id'));
        }

        if (isNaN(this.args[1])) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'))
        }

        const log = await this.knex('logs')
            .where('logs.id', '=', this.args[1])
            .join('users', 'users.id', '=', 'logs.user_id')
            .select([
                'logs.*',
                'users.is_sharing_logs'
            ])
            .first();

        if (!log) {
            return this.message.reply(sprintf(this.language.get('log_not_found_user'), this.args[1]));
        }

        if (log.user_id !== this.message.author.id && !log.is_sharing_logs) {
            return this.message.reply(sprintf(this.language.get('log_is_private')));
        }

        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setTitle(log.title ? log.title : sprintf(this.language.get('log_untitled'), log.id))
                .setDescription(log.content.length <= 2048 ? log.content : log.content.substr(0, 2045) + '...')
                .setURL(`https://defense-unit.delta-velorum.test/journey/${this.message.author.id}/${log.id}`)
                .setFooter(sprintf(this.language.get('log_meta'), moment(log.updated_at).format('MM/DD/YYYY hh:mm a'), log.id, log.user_id))
        )
    }

    async edit() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('log_edit_must_specify_id'));
        }

        if (isNaN(this.args[1])) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'))
        }

        const log = await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .first();

        if (!log) {
            return await this.message.reply(sprintf(this.language.get('log_not_found_user'), this.args[1]));
        }

        const content = this.args.slice(2).join(' ');

        await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .update({content: content});
        return this.message.reply(sprintf(this.language.get('log_edited_successfully')));
    }

    help() {
        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setDescription(this.language.get('log_what_is_it'))
                .addField('add', this.language.get('log_help_create'))
                .addField('edit', this.language.get('log_help_edit'))
                .addField('delete', sprintf(this.language.get('log_help_delete'), this.message.author.id))
                .addField('list', this.language.get('log_help_list'))
                .addField('view', this.language.get('log_help_view'))
                .addField('title', this.language.get('log_help_title'))
                // .addField('user', sprintf(this.language.get('log_help_user'), this.message.author.id))
                .setTitle(this.language.get('log_title'))
        )
    }

    async list() {
        if (isNaN(this.args[1]) && this.args[1] !== undefined) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), this.language.get('page_number')))
        }

        let page = this.args[1] ? Math.floor(this.args[1]) : 1;

        if (page <= 0) page = 1;

        const itemsPerPage = 10;

        let {pagination, data} = await this.knex('logs')
            .where({
                user_id: this.message.author.id,
            })
            .paginate({
                perPage: itemsPerPage,
                currentPage: page,
                isLengthAware: true
            });

        if (data.length === 0) {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('ORANGE')
                    .setAuthor(sprintf(this.language.get('log_list_title'), this.message.author.tag), this.message.author.displayAvatarURL())
                    .setDescription(this.language.get('no_entries'))
            )
        }

        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setAuthor(sprintf(this.language.get('log_list_title'), this.message.author.tag), this.message.author.displayAvatarURL())
                .setTitle(this.language.get('show_log_command'))
                .setDescription(
                    data.map(log => (log.title ? `#${log.id} - ${log.title}` : sprintf(this.language.get('log_untitled'), log.id))
                    ).join('\n')
                )
                .setFooter(sprintf(this.language.get('log_list_footer'), pagination.from, pagination.to, pagination.currentPage, pagination.lastPage))
        )
    }

    // async user() {
    //     if (isNaN(this.args[2]) && this.args[2] !== undefined) {
    //         return this.message.reply(sprintf(this.language.get('must_be_numeric'), this.language.get('page_number')))
    //     }
    //     let page = this.args[2] ? Math.floor(this.args[2]) : 1;
    //     if (page <= 0) page = 1;
    //
    //     const itemsPerPage = 10;
    //     let user = null;
    //
    //     try {
    //         user = this.message.mentions.users.first() || await this.client.users.fetch(this.args[1]);
    //     } catch (e) {
    //         return await this.message.reply(sprintf(this.language.get('log_user_not_found'), this.args[1]));
    //     }
    //
    //     if (!user) {
    //         return this.message.reply(sprintf(this.language.get('log_user_not_found'), this.args[1]));
    //     }
    //
    //     let author = await this.knex('users').where({id: user.id}).first();
    //
    //     if (!author || (!author.is_sharing_logs && user.id !== this.message.author.id)) {
    //         return this.message.reply(sprintf(this.language.get('log_target_is_private'), user.tag));
    //     }
    //
    //     let {pagination, data} = await this.knex('logs')
    //         .where({
    //             user_id: user.id,
    //         })
    //         .paginate({
    //             perPage: itemsPerPage,
    //             currentPage: page,
    //             isLengthAware: true
    //         });
    //
    //     if (data.length === 0) {
    //         return this.message.reply(
    //             new MessageEmbed()
    //                 .setColor('ORANGE')
    //                 .setAuthor(sprintf(this.language.get('log_list_title'), user.tag), user.displayAvatarURL())
    //                 .setDescription(this.language.get('no_entries'))
    //         )
    //     }
    //
    //     return this.message.reply(
    //         new MessageEmbed()
    //             .setColor('GREEN')
    //             .setAuthor(sprintf(this.language.get('log_list_title'), user.tag), user.displayAvatarURL())
    //             .setTitle(this.language.get('show_log_command'))
    //             .setDescription(
    //                 data.map(log => (log.title ? `#${log.id} - ${log.title}` : sprintf(this.language.get('log_untitled'), log.id))
    //                 ).join('\n')
    //             )
    //             .setFooter(sprintf(this.language.get('log_user_footer'), pagination.from, pagination.to, pagination.currentPage, pagination.lastPage))
    //     )
    // }

    async delete() {
        if (!this.args[1]) {
            return this.message.reply(this.language.get('log_title_must_specify_id'));
        }

        const log = await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .first();

        if (!log) {
            return await this.message.reply(sprintf(this.language.get('log_not_found_user'), this.args[1]));
        }

        await this.knex('logs')
            .where({id: this.args[1], user_id: this.message.author.id})
            .delete();

        return this.message.reply(sprintf(this.language.get('log_deleted'), log.id));
    }
};