const {MessageCollector, MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;

module.exports = class TradingManager {
    constructor(client, args, message, settings, language) {
        this.client = client;
        this.args = args;
        this.knex = client.knex;
        this.message = message;
        this.guild = message.guild;
        this.settings = settings;
        this.language = language;
    }

    async create() {
        let channel = await this.knex('channels').where({
            guild_id: this.guild.id,
            type: 'trade'
        }).first();

        if (!channel) {
            return this.message.reply(this.language.get('trade_has_no_channel'));
        }

        // This will collect the have & want for this trade
        let collector = new MessageCollector(
            this.message.channel,
            x => x.author.id === this.message.author.id, // only listen to the author's messages
            {
                max: 2,
                time: 12e4
            }
        );

        await this.message.reply(sprintf(this.language.get('create_trade_have'), 12e4 / 1e3));

        let step = 0;

        collector.on('collect', async (message) => {
            if (message.content.toLowerCase() === 'cancel') {
                await message.reply(this.language.get('trade_creation_cancelled'));
                return collector.stop('cancelled');
            }

            if (step === 0) {
                await message.reply(this.language.get('create_trade_want'));
                step++;
            }
        });

        collector.on('end', async (collected, reason) => {
            if (reason === 'limit') {
                await this.knex.raw('INSERT IGNORE INTO users (id, name) values (?, ?)', [this.message.author.id, this.message.author.tag]);


                const have = collected.first().content.substr(0, 1024);
                const want = collected.last().content.substr(0, 1024);
                const regex = /discordapp\.com|discord\.gg|discord.com|top\.gg|discordbots\.com/gi;

                if (regex.test(want) || regex.test(have)) {
                    return this.message.reply(this.language.get('trade_prohibited_words'));
                }

                let row = await this.knex('trades').insert({
                    have: have,
                    want: want,
                    user_id: this.message.author.id,
                    guild_id: this.guild.id,
                });

                await this.message.reply(sprintf(this.language.get('trade_creation_successful'), row[0]));

                channel = this.guild.channels.resolve(channel.id);

                if (!channel) {
                    return;
                }

                if (!channel.permissionsFor(this.guild.me).has('SEND_MESSAGES')) {
                    return;
                }

                let {id} = await channel.send(`<@${this.message.author.id}>`,
                    {
                        embed: new MessageEmbed()
                            .setColor('GREEN')
                            .setDescription([
                                '```',
                                sprintf(this.language.get('trade_user'), this.message.author.tag, this.message.author.id),
                                sprintf(this.language.get('trade_id'), row[0]),
                                '```',
                                sprintf(this.language.get('trade_send_message_to_user'), this.message.author.tag)
                            ].join('\n'))
                            .addField(this.language.get('trade_have'), have)
                            .addField(this.language.get('trade_want'), want)
                            .setThumbnail(this.message.author.displayAvatarURL())
                    }
                );

                await this.knex('trades').where({id: row[0]}).update({
                    message_id: id,
                });

                return true;
            }
        });
    }

    async view() {
        if (isNaN(this.args[1])) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'))
        }

        let trade = await this.knex('trades').where({
            id: this.args[1],
            guild_id: this.guild.id
        }).first();

        if (!trade) {
            return this.message.reply(sprintf(this.language.get('trade_not_found'), this.args[1]));
        }

        const owner = await this.client.users.fetch(trade.user_id);

        return this.message.reply(
            new MessageEmbed()
                .setColor(trade.status === 'open' ? 'GREEN' : 'RED')
                .setDescription([
                    '```',
                    sprintf(this.language.get('trade_user'), owner.tag, owner.id),
                    sprintf(this.language.get('trade_id'), trade.id),
                    sprintf(this.language.get('trade_status'), trade.status),
                    '```',
                    sprintf(this.language.get('trade_send_message_to_user'), owner.tag)
                ].join('\n'))
                .addField(this.language.get('trade_have'), trade.have)
                .addField(this.language.get('trade_want'), trade.want)
                .setThumbnail(owner.displayAvatarURL())
        )
    }

    async list() {
        if (isNaN(this.args[1]) && this.args[1] !== undefined) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), this.language.get('page_number')))
        }

        let page = this.args[1] ? Math.floor(this.args[1]) : 1;
        if (page <= 0) page = 1;

        const itemsPerPage = 10;

        let {pagination, data} = await this.knex('trades')
            .where({
                guild_id: this.guild.id,
                status: 'open'
            })
            .paginate({
                perPage: itemsPerPage,
                currentPage: page,
                isLengthAware: true
            });

        if (data.length === 0) {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('ORANGE')
                    .setAuthor(sprintf(this.language.get('trade_list_title'), this.guild.name), this.guild.iconURL())
                    .setDescription(this.language.get('no_entries'))
            )
        }

        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setAuthor(sprintf(this.language.get('trade_list_title'), this.guild.name), this.guild.iconURL())
                .setTitle(this.language.get('show_trade_command'))
                .setDescription(
                    data.map(x => {
                        let characters = sprintf(this.language.get('trading_pagination_entry'), x.id, x.user_id).length; // this will calculate the string length so we can use all 2048 characters
                        let remaining = (2048 / itemsPerPage) - (characters - 2); // the remaining characters that can be used.
                        return sprintf(this.language.get('trading_pagination_entry'), x.id, x.user_id, x.have.substr(0, remaining) + '..');
                    }).join('\n')
                )
                .setFooter(sprintf(this.language.get('trade_list_footer'), pagination.from, pagination.to, pagination.currentPage, pagination.lastPage))
        )
    }

    async user() {
        if (isNaN(this.args[2]) && this.args[2] !== undefined) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'page number'))
        }

        let page = this.args[2] ? Math.floor(this.args[2]) : 1;
        if (page <= 0) page = 1;

        const user = this.message.mentions.users.first() || await this.client.users.resolve(this.args[1]) || await this.client.users.fetch(this.args[1]);

        if (!user) {
            return this.message.reply(sprintf(this.language.get('trade_user_not_found'), this.args[1]));
        }

        const itemsPerPage = 10;

        let {pagination, data} = await this.knex('trades')
            .where({
                guild_id: this.guild.id,
                status: 'open',
                user_id: user.id,
            })
            .paginate({
                perPage: itemsPerPage,
                currentPage: page,
                isLengthAware: true
            });

        if (data.length === 0) {
            return this.message.reply(
                new MessageEmbed()
                    .setColor('ORANGE')
                    .setAuthor(sprintf(this.language.get('trade_list_title'), user.tag), user.displayAvatarURL())
                    .setDescription(this.language.get('no_entries'))
            )
        }

        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setAuthor(sprintf(this.language.get('trade_list_title'), user.tag), user.displayAvatarURL())
                .setTitle(this.language.get('show_trade_command'))
                .setDescription(
                    data.map(x => {
                        let characters = sprintf(this.language.get('trading_pagination_entry'), x.id, x.user_id).length; // this will calculate the string length so we can use all 2048 characters
                        let remaining = (2048 / itemsPerPage) - (characters - 2); // the remaining characters that can be used.
                        return sprintf(this.language.get('trading_pagination_entry'), x.id, x.user_id, x.have.substr(0, remaining) + '..');
                    }).join('\n')
                )
                .setFooter(sprintf(this.language.get('trade_user_footer'), pagination.from, pagination.to, pagination.currentPage, pagination.lastPage))
        )
    }

    async close() {
        if (isNaN(this.args[1])) {
            return this.message.reply(sprintf(this.language.get('must_be_numeric'), 'ID'))
        }

        let trade = await this.knex('trades').where({
            id: this.args[1],
            guild_id: this.guild.id
        }).first();

        if (!trade) {
            return this.message.reply(sprintf(this.language.get('trade_not_found'), this.args[1]));
        }

        if (trade.user_id !== this.message.author.id && !this.message.member.permissions.has('MANAGE_MESSAGES')) {
            return this.message.reply(this.language.get('trade_close_not_allowed'));
        }

        if (trade.status === 'closed') {
            return this.message.reply(sprintf(this.language.get('trade_already_closed'), trade.id))
        }

        await this.knex('trades').where({
            id: this.args[1],
            guild_id: this.guild.id
        }).update({
            status: 'closed'
        });

        if (trade.message_id) {
            let channel = await this.knex('channels').where({
                guild_id: this.guild.id,
                type: 'trade'
            }).first();

            if (channel) {
                channel = this.guild.channels.resolve(channel.id);
                if (channel) {
                    let message = await channel.messages.fetch(trade.message_id);
                    if (message && message.deletable) {
                        await message.delete({
                            reason: `Trade closed by ${this.message.author.tag}`
                        })
                    }
                }
            }
        }

        return this.message.reply(sprintf(this.language.get('trade_closed'), trade.id))
    }

    help() {
        return this.message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setDescription(this.language.get('trading_what_is_it'))
                .addField('create', this.language.get('trade_help_create'))
                .addField('close', sprintf(this.language.get('trade_help_close'), this.message.author.id))
                .addField('list', this.language.get('trade_help_list'))
                .addField('view', this.language.get('trade_help_view'))
                .addField('user', sprintf(this.language.get('trade_help_user'), this.message.author.id))
                .setTitle(this.language.get('trade_help_title'))
        )
    }

    async channel() {
        if (!this.args[1]) {
            let channel = await this.knex('channels').where({
                guild_id: this.guild.id,
                type: 'trade'
            }).first();

            if (!channel) {
                return this.message.reply(this.language.get('trade_channel_not_set'));
            }

            return this.message.reply(sprintf(this.language.get('trade_channel_name'), channel.id))
        }

        if (!this.message.member.permissions.has('MANAGE_GUILD')) {
            return this.message.reply(this.language.get('trade_channel_must_be_admin'))
        }

        let channel = this.message.mentions.channels.first() || this.guild.channels.resolve(this.args[1]);

        if (!channel) {
            return this.message.reply(sprintf(this.language.get('trade_channel_unknown'), this.args[1]));
        }

        await this.knex('channels').where({
            guild_id: this.guild.id,
            type: 'trade'
        }).delete();

        await this.knex('channels').insert({
            guild_id: this.guild.id,
            id: channel.id,
            type: 'trade'
        });

        return this.message.reply(sprintf(this.language.get('trade_channel_successfully_set'), channel.id))
    }
};