const languages = require('../lang');

class Language {
    constructor(language) {
        this.locale = language;

        if (languages[language]) {
            this.translations = languages[language];
        } else {
            this.translations = languages.en;
        }
    }

    /**
     * Get a translation, or fallback to english
     * @param key
     * @returns string
     */

    get(key) {
        if (this.translations[key]) {
            return this.translations[key];
        } else if (languages.en[key]) {
            return languages.en[key];
        } else {
            console.log(`Missing translation ${key} for locale ${this.locale}`);
            return key;
        }
    }

    /**
     * Check if a translation exists in the file
     * @param key
     * @param fallback
     * @returns {boolean}
     */

    has(key, fallback = true) {
        if (this.translations[key]) {
            return true;
        } else if (languages.en[key] && fallback) {
            return true;
        } else {
            return false;
        }
    }
}

module.exports = Language;