const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const creatures = require(__dirname + '/../json/creatures');
const passive = require(__dirname + '/../json/passive');
const {Creature, CreatureFood, CreatureTaming} = require('../support/Ark');
const sprintf = require('sprintf-js').sprintf;
const parse = require('yargs-parser');
const Arguments = require('../util/Arguments');

module.exports = {
    name: 'tame',
    description: 'Calculate taming requirements for a creature.',
    usage: 'tame [level=150] <creature> [--multiplier=1]',
    example: [',tame 130 Rex', ',tame 140 yutty --multiplier=4'],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        let level = settings.default_level || 150;
        let multiplier = settings.default_multiplier || 1;

        let arguments = new Arguments(args.join(' '));
        let name = arguments._

        if (arguments.has('multiplier') && !isNaN(arguments.get('multiplier'))) {
            multiplier = parseFloat(arguments.get('multiplier'));
        } else if (arguments.has('m') && !isNaN(arguments.get('multiplier'))) {
            multiplier = parseFloat(arguments.get('multiplier'));
        }

        if (!isNaN(args[0])) {
            level = parseInt(args[0]);
            name = name.slice(1);
        }

        if (level < 1 || level > 9000) {
            level = 150;
        }

        if (multiplier < 1 || multiplier > 500) {
            multiplier = 1;
        }

        // This looks for the creature name
        const searcher = new FuzzySearch(creatures, ['name', 'alias'], {
            sort: true
        });

        name = name.join(' ');

        let creature = searcher.search(name);
        if (!creature[0]) return message.reply(sprintf(language.get('tame_creature_not_found'), name));
        try {
            creature = new Creature(creature[0].name); // set the first found matching value as the creature
        } catch (e) {
            return message.reply(
                new MessageEmbed()
                    .setColor('RED')
                    .setDescription('An error occurred during the execution of the command. Please report this to the developers.')
                    .addField('Error', '```' + e.message + '```')
                    .addField('Message', message.content.substr(0, 1024))
            )
        }

        if (passive.includes(creature.name)) {
            return message.reply(language.get('unsupported_passive_tame'));
        }

        let embed = new MessageEmbed();
        embed.setColor('GREEN');
        let lines = [];

        embed.setTitle(sprintf(language.get('taming_title'), creature.name));

        embed.setDescription([
            `TamingSpeedMultiplier=${multiplier}`,
            `CreatureLevel=${level}`,
            '```yaml\n' + `,tame ${level} ${creature.name} --multiplier=${multiplier}` + '```'
        ]);

        const kibblePriorities = ['Exceptional Kibble', 'Extraordinary Kibble', 'Superior Kibble', 'Regular Kibble', 'Simple Kibble', 'Basic Kibble'];
        let alreadyHasBestKibble = false;

        creature.food.specialFoodValues.forEach((food) => {
            let quantity = CreatureFood.foodAmountNeeded(creature, level, food.name, multiplier);
            let duration = CreatureFood.tamingDuration(creature, quantity, food);

            let {effectiveness, levels} = CreatureTaming.effectiveness(creature, food, level, multiplier);

            if (!/(augmented)|(egg)/i.test(food.displayName)) {
                lines.push({
                    quantity: quantity,
                    food: food.displayName,
                    duration: duration.format('HH:mm:ss'),
                    effectiveness: effectiveness.toFixed(1),
                    levels: levels
                });
            }
        });

        lines = lines.sort((x, y) => x.quantity - y.quantity);

        lines.slice(0, 9).forEach(line => {
            if (kibblePriorities.includes(line.food) && alreadyHasBestKibble) {
                return;
            } else if (kibblePriorities.includes(line.food)) {
                alreadyHasBestKibble = true;
            }

            embed.addField(`**${line.quantity}x ${line.food}**`, [
                sprintf(language.get('taming_duration'), line.duration),
                sprintf(language.get('taming_effectiveness'), line.effectiveness),
                sprintf(language.get('taming_levels'), line.levels, level + line.levels),
            ], true);
        });

        return message.reply(embed);
    }
};
