const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const items = require(__dirname + '/../json/shop');
const sprintf = require('sprintf-js').sprintf;
const Arguments = require('../util/Arguments');

module.exports = {
    name: 'buy',
    description: 'Shows the price and quantity to buy items from the Hexagon Exchange',
    usage: 'buy [quantity=150] <item>',
    examples: [
        ',buy 20 Element'
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('hexagon_specify_item'));
        }

        let arguments = new Arguments(args.join(' '));
        let name = arguments._; // Arguments that were given with the command

        let quantity = 1;

        if (!isNaN(args[0])) {
            quantity = parseInt(args[0]);
            name = name.slice(1);
        }

        if (quantity < 1 || quantity > 1_000_000) {
            quantity = 1;
        }

        // This looks for the item name
        const searcher = new FuzzySearch(items, ['item'], {
            sort: true
        });

        name = name.join(' ');
        let item = searcher.search(name)[0];

        if (!item) {
            return message.reply(sprintf(language.get('hexagon_cant_buy_this'), name));
        }

        let embed = new MessageEmbed();

        embed.setColor('GREEN');

        embed.setTitle(sprintf(language.get('hexagon_title'), item.item, quantity));

        let lines = [
            `**${language.get('hexagon_cost')}**: ${quantity * item.cost} hexagons`,
            `**${language.get('hexagon_quantity')}**: ${quantity * item.quantity} ${item.item}`,
        ];

        embed.setDescription(lines.join('\n'));

        if (item.image) {
            embed.setThumbnail(item.image);
        }

        return message.reply(embed);
    }
};