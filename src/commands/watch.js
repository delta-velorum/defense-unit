const {Battlemetrics, Dedicated} = require('../support/GameServer');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'watch',
    description: 'Automatically update ARK server data. To find your server ID see `,help server`.',
    usage: 'watch <platform=pc,ps4,switch,mobile,xbox> <server id>',
    examples: [
        ',watch switch 3'
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    premium: true,
    execute: async function (client, message, args, settings, language) {
        if (!message.member.permissions.has('MANAGE_GUILD')) {
            return message.reply(
                language.get('watch_server_no_perms')
            );
        }

        if (!args[1]) {
            return message.reply(
                language.get('watch_server_args')
            )
        }

        let platform = args[0].toLowerCase();
        let id = args[1];

        let exists = await client.knex('watched_servers').where({
            guild_id: message.guild.id,
            server_id: args[1]
        }).first();

        if (exists) {
            return message.reply(language.get('watch_add_exists'));
        }

        let channel = await client.knex('channels')
            .where({guild_id: message.guild.id, type: 'server'})
            .first();

        if (!channel) {
            return message.reply(language.get('watch_must_set_channel'));
        }

        if (platform === 'pc') {
            try {
                let server = await new Battlemetrics(id);

                await client.knex('watched_servers').insert({
                    guild_id: message.guild.id,
                    server_id: server.id,
                    platform: platform,
                    name: server.name,
                });

                return message.reply(
                    sprintf(language.get('watch_server_add_success'), server.name)
                );
            } catch (e) {
                return message.reply(
                    sprintf(language.get('watch_server_add_failed'), e.message)
                )
            }
        } else {
            try {
                let server = await new Dedicated(id);

                await client.knex('watched_servers').insert({
                    guild_id: message.guild.id,
                    server_id: server.id,
                    platform: platform,
                    name: server.name,
                });

                return message.reply(
                    sprintf(language.get('watch_server_add_success'), server.name)
                )
            } catch (e) {
                return message.reply(
                    sprintf(language.get('watch_server_add_failed'), e.message)
                )
            }
        }
    }
};