const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const kibbles = require(__dirname + '/../json/kibbles');
const sprintf = require('sprintf-js').sprintf;
const Arguments = require('../util/Arguments');

module.exports = {
    name: 'kibble',
    examples: [
        ',kibble 20 rex',
        ',kibble 20 basic',
        ',kibble superior',
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('kibble_specify_name'));
        }

        let arguments = new Arguments(args.join(' '));
        let name = arguments._; // Arguments that were given with the command

        let quantity = 1;

        if (!isNaN(args[0])) {
            quantity = parseInt(args[0]);
            name = name.slice(1);
        }

        if (quantity < 1 || quantity > 1000) {
            quantity = 1;
        }

        // This looks for the item name
        const searcher = new FuzzySearch(kibbles, ['name', 'fullName', 'preferred'], {
            sort: true
        });

        name = name.join(' ');
        let kibble = searcher.search(name)[0];

        if (!kibble) {
            return message.reply(sprintf(language.get('kibble_not_found'), name));
        }

        let embed = new MessageEmbed();

        embed.setColor('GREEN');

        embed.setTitle(sprintf(language.get('craft_kibble'), kibble.fullName, quantity));
        embed.setDescription(kibble.recipe.map(x => `**${quantity * x.quantity}x** ${x.name}`).join('\n'));
        embed.setThumbnail(`https://ark.delta-velorum.com/images/kibbles/${kibble.name}.png`)

        embed.addField(language.get('kibble_preferred'), kibble.preferred.map(x => x.toLowerCase().includes(name.toLowerCase()) ? `**${x}**` : x).join(', '));
        embed.addField(language.get('kibble_dropped_by'), kibble.creatures.join(', '));

        return message.reply(embed);
    }
};