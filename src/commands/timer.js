const {MessageEmbed} = require('discord.js');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);
const parse = require(__dirname + '/../helpers/duration');
const TimerManager = require(__dirname + '/../util/TimerManager');


module.exports = {
    name: 'timer',
    description: 'Set a reminder in this channel. e.g tame/cuddle timer',
    usage: 'timer <time> <message>',
    examples: [
        ',timer 8 hours imprint argentavis',
        ',remindme in 1 hour 23 minutes rex tame about to wake up!',
        ',timer 10m 30s eggs about to hatch'
    ],
    aliases: ['remindme'],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    premium: false,
    execute: async function (client, message, args, settings, language) {
        let manager = new TimerManager(client, args, message, settings, language);

        if (typeof manager[args[0]] === 'function') {
            return manager[args[0]]();
        } else {
            let time = parse(args.join(' '), 's');

            if (!time.result || !time.timeStr || !time.str) {
                return client.commands.get('help').execute(client, message, ['timer'], settings, language);
            }

            if (time.result < 60) {
                return message.reply(language.get('timer_min_amount'));
            }

            let date = moment().add(time.result, 'seconds');

            let timers = await client.knex('reminders')
                .where({
                    user_id: message.author.id,
                })
                .where('run_at', '>=', 'NOW()');

            if (timers.length > 10) {
                return message.reply(
                    language.get('timer_max_limit')
                );
            }

            await client.knex('reminders').insert({
                message: time.str.substr(0, 1024),
                channel_id: message.channel.id,
                user_id: message.author.id,
                run_at: date.format('YYYY-MM-DD HH:mm:ss'),
            });

            return message.reply(
                new MessageEmbed()
                    .setColor('GREEN')
                    .setDescription(`I will remind you in **${time.timeStr.join(' ')}** to **${time.str}**`)
            );
        }
    }
};