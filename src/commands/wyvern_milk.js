const {MessageEmbed} = require('discord.js');
const format = require('../util/format');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'milk',
    description: 'Calculate the time before you can feed a Wyvern again.',
    usage: 'milk <current_food_number>',
    aliases: ['wm'],
    examples: [
        ',milk 2344',
        ',milk 1837 5',
    ],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('milk_must_be_positive'))
        }

        const multiplier = args[1] ? parseFloat(args[1]) : settings.default_breeding_multiplier || 1;
        const amount = parseFloat(args[0]);

        if (amount > 1e6) {
            return message.reply(sprintf(language.get('milk_max_number'), 1e6))
        }


        let embed = new MessageEmbed();
        embed.setColor('GREEN');
        embed.setTitle(language.get('milk_title'));

        const seconds = Math.abs((amount / multiplier) * 10); //Food * 10 = time in seconds
        let string = sprintf(language.get('milk_description'), amount.toFixed(2), format.toTimeString(seconds, language));

        embed.setDescription(string);
        embed.setThumbnail('https://ark.delta-velorum.com/images/icons/Wyvern_Milk_(Scorched_Earth).png')

        return await message.reply(embed); // send the embed!
    }
};