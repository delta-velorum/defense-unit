const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'support',
    description: 'Questions? We\'ve got an answer!',
    usage: 'support',
    examples: ',support',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setDescription(sprintf(language.get('support_description'), 'http://discord.gg/JBgQY53'))
        );
    }
};