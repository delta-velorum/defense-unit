const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const items = require(__dirname + '/../json/items');
const sprintf = require('sprintf-js').sprintf;
const Arguments = require('../util/Arguments');

module.exports = {
    name: 'craft',
    description: 'Show the recipe to craft an item.',
    usage: 'craft [quantity=1] <item>',
    examples: [
        'craft 4 auto turret',
        ',craft 80 arb',
        'craft 1 rex saddle'
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('craft_specify_name'));
        }

        let arguments = new Arguments(args.join(' '));
        let name = arguments._; // Arguments that were given with the command

        let quantity = 1;

        if (!isNaN(args[0])) {
            quantity = parseInt(args[0]);
            name = name.slice(1);
        }

        if (quantity < 1 || quantity > 1_000_000) {
            quantity = 1;
        }

        // This looks for the item name
        const searcher = new FuzzySearch(items, ['aliases','name'], {
            sort: true
        });

        name = name.join(' ');
        console.log(searcher.search(name));
        let item = searcher.search(name)[0];

        if (!item) {
            return message.reply(sprintf(language.get('craft_item_not_found'), name));
        }

        let embed = new MessageEmbed();

        embed.setColor('GREEN');

        embed.setTitle(sprintf(language.get('craft_title'), item.name, quantity));

        let description = [];

        if (item.default_quantity > 1) {
            description.push(`**NOTE:** This item yields ${item.default_quantity} pieces per craft!\n`);
        }

        description = description.concat(item.ingredients.map(x => `**${quantity * x.quantity}x** ${x.item}`)).join('\n');

        embed.setDescription(description);

        let lines = [
            `**${language.get('craft_prerequisite')}**: ${item.prerequisites.length > 0 ? item.prerequisites.join(', ') : language.get('craft_prerequisite_none')}`,
            `**${language.get('craft_exp_gain')}**: ${item.experience * quantity} EXP`,
            `**${language.get('craft_level')}**: ${item.level}`,
            `**${language.get('craft_engram_points')}**: ${item.engram_points}`
        ];

        if (item.crafted_in && item.crafted_in.length > 0) {
            lines.push(`**${language.get('craft_crafted_in')}**: ${item.crafted_in.join(', ')}`)
        }

        if (item.image) {
            embed.setThumbnail(item.image);
        }

        embed.addField(language.get('craft_extra_info'), lines.join('\n'));

        return message.reply(embed);
    }
};