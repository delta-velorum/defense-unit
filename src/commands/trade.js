const TradingManager = require('../util/TradingManager');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'trade',
    description: 'Propose a new trade or view all the current trades in this server.',
    usage: 'trade help',
    aliases: [],
    guild: true,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        let manager = new TradingManager(client, args, message, settings, language);

        if (!args[0]) {
            args[0] = 'help';
        }

        // if the first argument is not a function in TradingManager, send an error message
        if (typeof manager[args[0].toLowerCase()] !== 'function') {
            return message.reply(sprintf(language.get('trade_unknown_action'), args[0]));
        }

        // run the function
        return manager[args[0].toLowerCase()]();
    }
};