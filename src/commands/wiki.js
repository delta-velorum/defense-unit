const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const creatures = require(__dirname + '/../json/dossiers');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'wiki',
    description: 'Displays useful information about a creature.',
    usage: 'wiki <creature>',
    example: [',wiki Rex'],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {

        const searcher = new FuzzySearch(creatures, ['name', 'alias'], {
            sort: true
        });

        let creature = searcher.search(args.join(' '))[0];

        if (!creature) {
            return message.reply(sprintf(language.get('wiki_creature_not_found'), args.join(' ')));
        }

        let embed = new MessageEmbed();

        if (creature.image) {
            embed.setImage(creature.image);
        }

        embed.setTitle(sprintf(language.get('wiki_title'), creature.name));
        embed.setURL(`https://ark.gamepedia.com/${creature.name.replace(/ /g, '_')}`);
        embed.setColor('GREEN');

        embed.setDescription([
            sprintf(language.get('wiki_saddle_level'), creature.saddleLevel == 0 ? 'No saddle needed' : creature.saddleLevel),
            sprintf(language.get('wiki_temperament'), creature.temperament),
            sprintf(language.get('wiki_diet'), creature.diet),
            sprintf(language.get('wiki_group'), creature.group),
            language.get('wiki_can_be_ridden') + `${creature.rideable ? language.get('yes') : language.get('no')}`,
            language.get('wiki_can_tamed') + `${creature.tameable ? language.get('yes') : language.get('no')}`,
            sprintf(language.get('wiki_feces_size'), creature.fecesSize),
            sprintf(language.get('wiki_entity_id'), creature.entityId),
        ])

        return message.reply(embed);
    }
};