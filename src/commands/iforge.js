const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);


module.exports = {
    name: 'iforge',
    description: 'Calculate the time to refine metal or oil in a refining forge.',
    usage: 'forge <amount of metal or oil> <metal or oil> [amount of forges]',
    aliases: ['if'],
    examples: [
        ',iforge 2600 metal',
        ',iforge 2600 metal 2',
    ],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[1]) {
            return message.reply(
                language.get('iforge_invalid_syntax')
            )
        }

        const fuel = {
            gasoline: 900,
        };

        const stacks = {
            metal: {
                size: 40,
                time: 20,
                equals: 20
            },
            scrap: {
                size: 100,
                time: 30,
                equals: 100
            },
            oil: {
                size: 120,
                time: 30,
                equals: 100,
            },
            hide: {
                size: 100,
                time: 30,
                equals: 100,
            },
            wood: {
                size: 1,
                time: 1,
                equals: 1
            }
        }

        let quantity = parseFloat(args[0]);

        let type = args[1].toLowerCase();

        let forges = parseInt(args[2]) || 1

        if (!['metal', 'oil', 'hide', 'wood', 'scrap'].includes(type)) {
            return message.reply(
                language.get('iforge_invalid_material')
            )
        }

        if (quantity < stacks[type].size) {
            return message.reply(
                sprintf(language.get('iforge_min_amount'), stacks[type].size)
            )
        }


        if (forges > 5000 || forges < 1) {
            forges = 1;
        }

        let metal = (quantity) => {
            // amount of batches that can be melted
            let amount = Math.floor(quantity / stacks.metal.size); // 40 is the amount of metal that can be melted

            return {
                amount: amount * stacks.metal.equals,
                time: (amount * stacks.metal.time) / forges
            };
        }

        let scrap = (quantity) => {
            // amount of batches that can be melted
            let amount = Math.floor(quantity / stacks.scrap.size); // 40 is the amount of metal that can be melted

            return {
                amount: amount * stacks.scrap.equals,
                time: (amount * stacks.scrap.time) / forges
            };
        }

        let wood = (quantity) => {
            // amount of batches that can be melted
            let amount = Math.floor(quantity / stacks.wood.size); // 40 is the amount of metal that can be melted

            return {
                amount: amount * stacks.wood.equals,
                time: (amount * stacks.wood.time) / forges
            };
        }

        let gasoline = (quantity) => {
            if (type === 'oil') {
                const amount = Math.floor(quantity / stacks.oil.size);

                return {
                    amount: amount * stacks.oil.equals,
                    time: (amount * stacks.oil.time) / forges,
                    hide: amount * 100
                };
            } else {
                const amount = Math.floor(quantity / stacks.hide.size);

                return {
                    amount: amount * stacks.hide.equals,
                    time: (amount * stacks.hide.time) / forges,
                    oil: amount * 120,
                };
            }
        }

        let embed = new MessageEmbed();

        embed.setTitle(language.get('iforge_title'));
        embed.setThumbnail('https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c5/Industrial_Forge.png');
        embed.setColor('GREEN');
        embed.setFooter('Available types: scrap, metal, oil, hide')

        if (type === 'metal') {
            let forge = metal(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            embed.setDescription(
                sprintf(language.get('iforge_metal'), time, quantity, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Gasoline:744957235993640971> Gasoline: ${Math.ceil(forge.time / fuel.gasoline)}`,
            ]);
        } else if (type === 'scrap') {
            let forge = scrap(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            embed.setDescription(
                sprintf(language.get('iforge_scrap'), time, quantity, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Gasoline:744957235993640971> Gasoline: ${Math.ceil(forge.time / fuel.gasoline)}`,
            ]);
        } else if (type === 'wood') {
            let forge = wood(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            embed.setDescription(
                sprintf(language.get('iforge_wood'), time, quantity, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Gasoline:744957235993640971> Gasoline: ${Math.ceil(forge.time / fuel.gasoline)}`,
            ]);
        } else {
            let forge = gasoline(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            console.log(forge);

            embed.setDescription(
                type === 'oil' ?
                    sprintf(language.get('forge_oil'), time, quantity, forge.hide, forge.amount, forges)
                    :
                    sprintf(language.get('forge_hide'), time, quantity, forge.oil, forge.amount, forges)
            );
            embed.addField('Fuel', [
                `<:Gasoline:744957235993640971> Gasoline: ${Math.ceil(forge.time / fuel.gasoline)}`,
            ]);
        }

        return message.reply(embed);
    }
};