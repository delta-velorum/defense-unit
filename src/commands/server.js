const {Manager, Battlemetrics, Dedicated} = require('../support/GameServer');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'server',
    description: 'View data about an ARK: Survival Evolved server.',
    usage: 'server help',
    aliases: [],
    guild: true,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            args[0] = 'help';
        }

        const manager = new Manager(client, args, message, settings, language);

        if (typeof manager[args[0].toLowerCase()] === 'function') {
            return manager[args[0].toLowerCase()]();
        } else {
            if (!args[0]) {
                return message.reply(language.get('server_must_specify_id'))
            }

            let server = await client.knex('servers')
                .where({server_id: args[0]})
                .orWhere('name', 'like', `%${args.join(' ')}%`)
                .andWhere({guild_id: message.guild.id})
                .first();

            if (!server) {
                return message.reply(sprintf(language.get('server_not_found'), args.join(' ')))
            }

            if (server.platform === 'pc') {
                let api = await new Battlemetrics(server.server_id);

                if (api.name !== server.name) {
                    await client.knex('servers').where({guild_id: message.guild.id, server_id: api.id}).update({name: api.name});
                }

                return api.toEmbed(message, language);
            } else {
                let api = await new Dedicated(server.server_id);

                if (api.name !== server.name) {
                    await client.knex('servers').where({guild_id: message.guild.id, server_id: api.id}).update({name: api.name});
                }

                return api.toEmbed(message, language);
            }
        }
    }
};