const {MessageEmbed} = require('discord.js');
const {version, developers, contributors} = require('../../config');

module.exports = {
    name: 'contributors',
    description: 'A special thanks to these people!',
    usage: 'contributors',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            '```' +
            contributors.map(x => `${x.name} - ${x.contributions}.`).join('\n')
            +'```'
        );
    }
};