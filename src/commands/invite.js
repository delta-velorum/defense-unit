const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'invite',
    description: 'Add Defense Unit to your server!',
    usage: 'invite',
    example: [',invite'],
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .setDescription(sprintf(language.get('invite_description'), 'https://discord.com/oauth2/authorize?client_id=488442856353300513&scope=bot&permissions=281664', 'https://top.gg/bot/488442856353300513'))
        );
    }
};