const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const creatures = require(__dirname + '/../json/cloning');
const sprintf = require('sprintf-js').sprintf;
const Arguments = require('../util/Arguments');

module.exports = {
    name: 'clone',
    description: 'Calculate the cloning cost and time for the given creature.',
    usage: 'clone [level=150] <creature> [--multiplier=1]',
    examples: [
        ',clone 140 Rex',
        ',clone 140 thylla --multiplier=6',
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    premium: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('clone_must_specify_creature_name'));
        }

        let arguments = new Arguments(args.join(' '));
        let name = arguments._; // Arguments that were given with the command

        let level = 150;
        let multiplier = 1;

        // only applicable to guilds
        if (settings) {
            level = settings.level || level;
            multiplier = settings.baby_mature_speed || multiplier;
        }

        if (arguments.has('multiplier') && !isNaN(arguments.get('multiplier'))) {
            multiplier = parseFloat(arguments.get('multiplier'))
        } else if (arguments.has('m') && !isNaN(arguments.get('m'))) {
            multiplier = parseFloat(arguments.get('m'))
        }

        if (!isNaN(args[0])) {
            level = parseInt(args[0]);
            name = name.slice(1);
        }

        if (level < 1 || level > 9000) {
            level = 150;
        }

        if (multiplier < 1 || multiplier > 500) {
            multiplier = 1;
        }

        // This looks for the creature name
        const searcher = new FuzzySearch(creatures, ['name', 'alias'], {
            sort: true
        });


        name = name.join(' ');
        let creature = searcher.search(name);

        creature = creature[0]; // set the first found matching value as the creature

        if (!creature) {
            return message.reply(sprintf(language.get('clone_creature_not_found'), name));
        }

        const CloneElementCostPerLevelGlobalMultiplier = 5500; // fixed value from Cloning Chamber
        const CloneBaseElementCostGlobalMultiplier = 2500; // fixed value from Cloning Chamber
        const CloningTimePerElementShard = 7; // fixed value from Cloning Chamber

        let costPerLevel = creature.stats[1];
        let costForLevel = costPerLevel * level;
        let baseCost = creature.stats[0];

        let cloneCost = costForLevel + baseCost;

        let cloneTime =  (creature.stats[2] + creature.stats[3] * level) / multiplier;
        let cloneTimeReadable = new Date(cloneTime * 1000);

        let embed = new MessageEmbed();
        embed.setColor('GREEN');
        embed.setTitle(sprintf(language.get('clone_title'), creature.name)); // CreatureName | Level: 150 | Multiplier: 1
        embed.setDescription('```yaml\n' + `,clone ${level} ${creature.name} --multiplier=${multiplier}` + '\n```')
        embed.addField(
            language.get('clone_costs'),
            Math.ceil(cloneCost) + ' <:element_shard:734821427206946906>'
        );
        embed.addField(
            language.get('clone_time'),
            (cloneTimeReadable.getUTCDate() - 1) + 'd ' + cloneTimeReadable.getUTCHours() + 'h ' + cloneTimeReadable.getUTCMinutes() + 'm ' + cloneTimeReadable.getUTCSeconds() + 's'
        );

        return message.reply(embed); // send the embed!
    }
};