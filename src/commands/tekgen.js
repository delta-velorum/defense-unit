const {MessageEmbed} = require('discord.js');
const Arguments = require('../util/Arguments');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);


module.exports = {
    name: 'tekgen',
    description: 'Calculate the time before a tek generator runs out of element.',
    usage: 'tekgen <radius> <element> [shards]',
    aliases: ['tg', 'gen'],
    examples: [
        ',tekgen 1 4 (radius 1, 4 element)',
        ',tg 1 0 12 (radius 1, 0 element, 12 shards)',
    ],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        let radius = 1;
        let element = 1;
        let shards = 0;
        let consumption = 1;

        if (!args[0]) {
            return client.commands.get('help').execute(client, message, ['tg'], settings, language);
        }


        if (args[0]) {
            radius = parseInt(args[0]);
        }

        if (args[1]) {
            element = parseInt(args[1]);
        }

        if (args[2]) {
            shards = parseInt(args[2]);
        }

        let totalElement =  Math.abs(shards / 100) + element;

        let embed = new MessageEmbed();
        embed.setColor("GREEN");


        let arguments = new Arguments(args.join(' '));

        if (arguments.has('consumption') && !isNaN(arguments.get('consumption'))) {
            consumption = parseInt(arguments.get('consumption'));
        } else if (arguments.has('c') && !isNaN(arguments.has('c'))) {
            consumption = parseInt(arguments.get('c'));
        }

        if (totalElement <= 0) {
            return message.reply('gen_fuel_nan');
        }

        if (radius < 1 || radius > 30) {
            radius = 1;
        }

        if (consumption < 0 || consumption > 1000) {
            consumption = 1;
        }

        const baseRadius = Math.abs(1 + ((radius - 1) * 0.33));
        const elementPerHour = Math.abs(18 / baseRadius);
        let duration = moment.duration((Math.abs(totalElement * elementPerHour) * consumption) * 3600, 'seconds');

        embed.setTitle(sprintf(language.get('tek_generator_title'), element, shards));
        embed.setDescription([
            sprintf(language.get('tek_gen_runs_out_in'), duration.format('DD:HH:mm:ss', {
                trim: false
            })),
            '```yaml',
            `,tekgen ${radius} ${element} ${shards} --consumption=${consumption}`,
            `,tekgen <radius> <element> <shards>`,
            '```'
        ].join('\n'));

        return await message.reply(embed); // send the embed!
    }
};