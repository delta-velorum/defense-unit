const {MessageEmbed} = require('discord.js');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

module.exports = {
    name: 'venom',
    description: 'Calculate the time before a rock drake starves to death.',
    usage: 'venom <current food> <maturation>',
    aliases: [],
    examples: [
        ',venom 1233 23.4'
    ],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('venom_must_specify_food_amount'))
        }

        if (isNaN(args[0])) {
            return message.reply(language.get('venom_food_is_nan'))
        }

        if (!args[1]) {
            return message.reply(language.get('venom_must_specify_maturation'))
        }

        if (isNaN(args[1])) {
            return message.reply(language.get('venom_maturation_is_nan'))
        }

        let food = parseInt(args[0]);
        let maturation = parseFloat(args[1]);

        if (food < 1 || food > 100_000) {
            return message.reply(language.get('venom_too_large'))
        }

        if (maturation < 1 || maturation > 100) {
            return message.reply(language.get('venom_maturation_too_large'))
        }

        let time = 0;

        // from 50 to 100 percent they consume food every 45 seconds instead of every 10 seconds
        if (maturation >= 50) {
            time = food * 45;
        } else {
            time = food * 10;
        }

        const duration = moment.duration(time, 'seconds');

        let embed = new MessageEmbed();
        embed.setThumbnail('https://gamepedia.cursecdn.com/arksurvivalevolved_gamepedia/4/4d/Nameless_Venom_%28Aberration%29.png');
        embed.setTitle(language.get('venom_title'));
        embed.setDescription(
            [
                `**${language.get('venom_maturation')}**: ${maturation.toFixed(1)}%`,
                `**${language.get('venom_food')}**: ${food}\n`,
                `**${language.get('venom_time')}**: ${duration.format('DD:HH:mm:ss', {
                    trim: false
                })} (${(time / 60 / 60).toFixed(1)}hrs)`
            ]
        );
        embed.setColor('GREEN');


        return message.reply(embed); // send the embed!
    }
};