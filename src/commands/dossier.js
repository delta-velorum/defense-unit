const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const creatures = require(__dirname + '/../json/dossiers');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'dossier',
    description: 'Shows the dossier of a creature.',
    usage: 'dossier <creature>',
    examples: [
        ',dossier Rex'
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {

        const searcher = new FuzzySearch(creatures, ['name', 'alias'], {
            sort: true
        });

        let creature = searcher.search(args.join(' '))[0];

        if (!creature) {
            return message.reply(sprintf(language.get('wiki_creature_not_found'), args.join(' ')));
        }

        let embed = new MessageEmbed();

        if (creature.image) {
            embed.setImage(creature.image);
        }

        embed.setTitle(sprintf(language.get('dossier_title'), creature.name));
        embed.setURL(`https://ark.gamepedia.com/${creature.name.replace(/ /g, '_')}`)
        embed.setColor('GREEN');

        if (creature.wild.length > 0) {
            embed.addField(language.get('wiki_wild'), creature.wild.join('\n\n').substr(0, 1024));
        }

        if (creature.domesticated.length > 0) {
            embed.addField(language.get('wiki_domesticated'), creature.domesticated.join('\n\n').substr(0, 1024));
        }

        return message.reply(embed);
    }
};