const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'help',
    description: 'List all commands, or get information about a certain command',
    usage: 'help [command]',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (args[0]) {
            if (!client.commands.has(args[0].toLowerCase())) {
                return message.reply(sprintf(language.get('help_unknown_command'), args[0]));
            }

            let command = client.commands.get(args[0].toLowerCase());

            let embed = new MessageEmbed();

            embed.setColor('GREEN');
            embed.setTitle(sprintf(language.get('help_title_command'), command.name));
            embed.addField(language.get('help_cooldown'), command.rate_limit, true);
            embed.setDescription(command.description);
            embed.addField(language.get('help_guild_only'), command.guild ? language.get('yes') : language.get('no'), true);
            embed.addField(language.get('toggleable'), command.toggleable ? language.get('yes') : language.get('no'), true);
            embed.addField(language.get('premium'), command.premium ? language.get('yes') : language.get('no'), true);
            embed.addField(language.get('help_usage'), '```yaml\n' + settings.prefix + language.get('commands.kibble.usage') + '\n```', false);
            if (command.examples) {
                embed.addField(language.get('help_example'), '```yaml\n' + command.examples.map(x => {
                    if (x.startsWith(',')) {
                        return settings.prefix + x.slice(1)
                    } else {
                        return settings.prefix + x;
                    }
                }).join('\n') + '\n```', false);
            }

            embed.setThumbnail(client.user.displayAvatarURL());

            return message.reply(embed);
        } else {
            let embed = new MessageEmbed();

            const lines = [
                '!! **[V4 changes](https://ark.delta-velorum.com/v4)** !!',
                '[commands with examples](https://ark.delta-velorum.com/commands)',
                '',
                '\`Command\` - \`Aliases\`'
            ];

            for (const command of client.commands.commands.values()) {
                if (!command.hidden) {
                    if (command.aliases.length > 0) {
                        lines.push(
                            `${command.name} - ${command.aliases.join(', ')}`
                        )
                    } else {
                        lines.push(
                            `${command.name}`
                        )
                    }
                }
            }

            embed.setAuthor(sprintf(language.get('help_title'), client.user.username), client.user.displayAvatarURL());
            embed.setColor('GREEN');
            embed.setDescription(lines.join('\n'));
            embed.setThumbnail(client.user.displayAvatarURL());
            embed.addField('Having issues?', `The prefix for Defense Unit in this server is \`${settings.prefix}\`. To view information and examples for a command use \`${settings.prefix}help <command>\`. You can also contact support in our [support server](http://discord.gg/JBgQY53).`);

            return message.reply(embed);
        }
    }
};