module.exports = {
    name: 'unwatch',
    description: 'Remove a server from the watchlist.',
    usage: 'unwatch <server id>',
    examples: [
        ',unwatch 3'
    ],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    premium: true,
    execute: async function (client, message, args, settings, language) {
        if (!message.member.permissions.has('MANAGE_GUILD')) {
            return message.reply(
                language.get('watch_server_no_perms')
            );
        }

        if (!args[0]) {
	  return message.reply('You must specify a server ID')
        }

        let result = await client.knex('watched_servers')
            .where({
                guild_id: message.guild.id,
                server_id: args[0]
            })
            .delete();

        if (result > 0) {
            return message.reply(
                language.get('watch_remove_success')
            )
        } else {
            return message.reply(
                language.get('watch_remove_failed')
            )
        }
    }
};
