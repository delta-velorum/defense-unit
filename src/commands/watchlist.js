const {MessageEmbed} = require('discord.js');

module.exports = {
    name: 'watchlist',
    description: 'View all servers that are being watched.',
    usage: 'watchlist',
    examples: [],
    aliases: [],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    premium: true,
    execute: async function (client, message, args, settings, language) {
        if (!message.member.permissions.has('MANAGE_GUILD')) {
            return message.reply(
                language.get('watch_server_no_perms')
            );
        }

        let rows = await client.knex('watched_servers')
            .where({
                guild_id: message.guild.id,
            });

	console.log(rows)

        let embed = new MessageEmbed();

        embed.setDescription('```' + rows.map(x => `${x.name} (${x.server_id})`).join(' - ').substr(0, 2000) + '```');
        embed.setFooter(language.get('watch_unwatch_server'));
        embed.setColor('GREEN');

        return message.reply(embed);
    }
};
