const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);


module.exports = {
    name: 'forge',
    description: 'Calculate the time to refine metal or oil in a refining forge.',
    usage: 'forge <amount of metal or oil> <metal or oil> [amount of forges]',
    aliases: ['rf', 'refine'],
    examples: [
        ',forge 12 metal',
        ',forge 12 metal 8',
    ],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[1]) {
            return message.reply(
                language.get('forge_invalid_syntax')
            )
        }

        const fuel = {
            thatch: 7.5,
            wood: 30,
            spark: 60,
            gel: 240,
        }

        const stacks = {
            metal: {
                size: 2,
                time: 20,
                equals: 1
            },
            scrap: {
                size: 1,
                time: 15,
                equals: 1
            },
            oil: {
                size: 6,
                time: 30,
                equals: 5,
            },
            hide: {
                size: 5,
                time: 30,
                equals: 5,
            }
        }

        let quantity = parseFloat(args[0]);

        let type = args[1].toLowerCase();

        let forges = parseInt(args[2]) || 1

        if (!['metal', 'oil', 'hide', 'scrap'].includes(type)) {
            return message.reply(
                language.get('iforge_invalid_material')
            )
        }

        if (quantity < stacks[type].size) {
            return message.reply(
                sprintf(language.get('forge_min_amount'), stacks[type].size)
            )
        }

        if (forges > 5000 || forges < 1) {
            forges = 1;
        }

        let metal = (quantity) => {
            const amount = Math.floor(quantity / stacks.metal.size);

            return {
                amount: amount * stacks.metal.equals,
                time: (amount * stacks.metal.time) / forges
            };
        }

        let scrap = (quantity) => {
            const amount = Math.floor(quantity / stacks.scrap.size);

            return {
                amount: amount * stacks.scrap.equals,
                time: (amount * stacks.scrap.time) / forges
            };
        }

        let gasoline = (quantity) => {
            if (type === 'oil') {
                const amount = Math.floor(quantity / stacks.oil.size);

                return {
                    amount: amount * stacks.oil.equals,
                    time: (amount * stacks.oil.time) / forges,
                    hide: amount * 5
                };
            } else {
                const amount = Math.floor(quantity / stacks.hide.size);

                return {
                    oil: amount * 6,
                    amount: amount * stacks.hide.equals,
                    time: (amount * stacks.hide.time) / forges
                };
            }
        }

        let embed = new MessageEmbed();

        embed.setTitle(language.get('forge_title'));
        embed.setThumbnail('https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/98/Refining_Forge.png');
        embed.setColor('GREEN');
        embed.setFooter('Available types: scrap, metal, oil, hide')

        if (type === 'metal') {
            let forge = metal(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            embed.setDescription(
                sprintf(language.get('forge_metal'), time, quantity, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Thatch:771828528240656404> Thatch: ${Math.ceil(forge.time / fuel.thatch)}`,
                `<:Wood:771828527674818560> Wood: ${Math.ceil(forge.time / fuel.wood)}`,
                `<:Sparkpowder:771828527951511614> Sparkpowder: ${Math.ceil(forge.time / fuel.spark)}`,
                `<:AnglerGel:771828528295313478> Angler Gel: ${Math.ceil(forge.time / fuel.gel)}`,
            ]);
        } else if (type === 'scrap') {
            let forge = scrap(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            embed.setDescription(
                sprintf(language.get('forge_scrap'), time, quantity, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Thatch:771828528240656404> Thatch: ${Math.ceil(forge.time / fuel.thatch)}`,
                `<:Wood:771828527674818560> Wood: ${Math.ceil(forge.time / fuel.wood)}`,
                `<:Sparkpowder:771828527951511614> Sparkpowder: ${Math.ceil(forge.time / fuel.spark)}`,
                `<:AnglerGel:771828528295313478> Angler Gel: ${Math.ceil(forge.time / fuel.gel)}`,
            ]);
        } else {
            let forge = gasoline(quantity);
            let time = moment.duration(forge.time, 'seconds').format('H[h] m[m] s[s]');

            console.log(forge);

            embed.setDescription(
                type === 'oil'?
                    sprintf(language.get('forge_oil'), time, quantity, forge.hide, forge.amount, forges)
                    :
                    sprintf(language.get('forge_hide'), time, quantity, forge.oil, forge.amount, forges)
            );

            embed.addField('Fuel', [
                `<:Thatch:771828528240656404> Thatch: ${Math.ceil(forge.time / fuel.thatch)}`,
                `<:Wood:771828527674818560> Wood: ${Math.ceil(forge.time / fuel.wood)}`,
                `<:Sparkpowder:771828527951511614> Sparkpowder: ${Math.ceil(forge.time / fuel.spark)}`,
                `<:AnglerGel:771828528295313478> Angler Gel: ${Math.ceil(forge.time / fuel.gel)}`,
            ]);
        }

        return message.reply(embed);
    }
};