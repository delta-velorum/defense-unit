const {MessageEmbed} = require('discord.js');
const {version, developers, contributors} = require('../../config');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

module.exports = {
    name: 'info',
    description: 'Basic information about the bot',
    usage: 'info',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        let guilds = 0;
        try {
            guilds = await client.shard.fetchClientValues('guilds.cache.size');
            guilds = guilds.reduce((x, y) => x + y, 0);
        } catch (e) {
            return message.reply(
                new MessageEmbed()
                    .setColor('RED')
                    .setDescription('An error occurred during the execution of the command. Please report this to the developers.')
                    .addField('Error', '```' + e.message + '```')
                    .addField('Message', message.content.substr(0, 1024))
            )
        }

        let embed = new MessageEmbed();
        embed.setColor('GREEN');
        embed.setTitle(client.user.username);
        embed.setThumbnail(client.user.displayAvatarURL());

        let links = [
            '[ARK Server List](https://servers.delta-velorum.com)',
            '[Dashboard for server admins](https://ark.delta-velorum.com)',
        ]

        if (!settings.premium) {
            // embed.setDescription('Defense Unit is sponsored by [arkservers.io](https://arkservers.io/?r=gpsmja), try their **free** 8-hour trial (no credit card required!) or use code `DEFENSEUNIT` to get 20% off your first month. *[remove this message](https://ark.delta-velorum.com/premium)*');
            links.push('[arkservers.io - Server Hosting](https://arkservers.io/?r=gpsmja)')
        }

        embed.addField(language.get('bot_version'), version, true);
        embed.addField(language.get('servers'), guilds, true);
        embed.addField(language.get('shards'), `${client.shard.count} (#${client.shard.ids[0]})`, true);
        embed.addField(language.get('relevant_links'), links.join('\n'), true);
        embed.addField(language.get('developers'), developers.map(x => x.name).join('\n'), true);
        embed.addField(language.get('invite_url'), '[click me](https://discord.com/oauth2/authorize?client_id=488442856353300513&scope=bot&permissions=281664)', true);

        const readyAt = moment(client.readyAt);
        const now = moment();

        let duration = moment.duration(now.diff(readyAt, 'seconds'), 'seconds');

        embed.addField(language.get('uptime_shard'), duration.format('H:mm:ss', {
            trim: false,
        }));

        return message.reply(embed);
    }
};