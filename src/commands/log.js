const LogManager = require('../util/LogManager');
const sprintf = require('sprintf-js').sprintf;

module.exports = {
    name: 'log',
    description: 'A useful tool to log your journey, can also be used for notes!',
    usage: 'log help',
    aliases: ['journey', 'note'],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            args[0] = 'help';
        }

        let manager = new LogManager(client, args, message, settings, language);

        // if the first argument is not a function in LogManager, send an error message
        if (typeof manager[args[0].toLowerCase()] !== 'function') {
            return message.reply(sprintf(language.get('log_unknown_action'), args[0]));
        }

        // run the function
        return manager[args[0].toLowerCase()]();
    }
};