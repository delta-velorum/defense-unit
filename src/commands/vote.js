const {MessageEmbed} = require('discord.js');

module.exports = {
    name: 'vote',
    description: 'Vote for the bot to enter in giveaways',
    usage: 'vote',
    aliases: [],
    guild: true,
    toggleable: false,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
            return message.reply(
                new MessageEmbed()
                    .setDescription(
                        'Vote for Defense Unit on [top.gg](https://top.gg/bot/488442856353300513) to participate in giveaways. Giveaways happen every 50 votes and will contain an ARK game key or random DLC key for steam!'
                    ).setColor('GREEN')
            );
    }
};