const {MessageEmbed} = require('discord.js');
const FuzzySearch = require('fuzzy-search');
const creatures = require(__dirname + '/../json/creatures');
const {Creature} = require('../support/Ark');
const sprintf = require('sprintf-js').sprintf;
const Arguments = require('../util/Arguments');
const weapons = require(__dirname + '/../json/weapons');
const special_weapons = require(__dirname + '/../json/special_weapons');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

module.exports = {
    name: 'knockout',
    description: 'Calculate the knockout stats for this creature.',
    usage: 'ko [level=150] <creature>',
    example: [',ko 150 Rex'],
    aliases: ['ko'],
    guild: false,
    toggleable: true,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        let level = settings.default_level || 150;

        let arguments = new Arguments(args.join(' '));
        let name = arguments._;

        if (!isNaN(args[0])) {
            level = parseInt(args[0]);
            name = name.slice(1);
        }

        if (level < 1 || level > 9000) {
            level = 150;
        }

        // This looks for the creature name
        const searcher = new FuzzySearch(creatures, ['name', 'alias'], {
            sort: true
        });

        name = name.join(' ');

        let creature = searcher.search(name);
        if (!creature[0]) return message.reply(sprintf(language.get('tame_creature_not_found'), name));
        try {
            creature = new Creature(creature[0].name); // set the first found matching value as the creature
        } catch (e) {
            return message.reply(
                new MessageEmbed()
                    .setColor('RED')
                    .setDescription('An error occurred during the execution of the command. Please report this to the developers.')
                    .addField('Error', '```' + e.message + '```')
                    .addField('Message', message.content.substr(0, 1024))
            )
        }


        let embed = new MessageEmbed();
        embed.setColor('GREEN');

        let time = moment.duration(creature.taming.getWakeUpTime(level, creature.fullStatsRaw), 'seconds');
        let torpor = creature.taming.getTotalTorpor(level, creature.fullStatsRaw);

        let lines = [
            `**${language.get('knockout_wakes_up_in')}**: ${time.format('HH:mm:ss')}`
        ];

        for (const weapon of special_weapons.hasOwnProperty(creature.name) ? special_weapons[creature.name] : weapons) {
            const hits = Math.ceil(torpor / (weapon.torpor.instant + weapon.torpor.overtime));
            const damage = (hits * (weapon.damage.instant + weapon.damage.overtime)).toFixed(2);

            lines.push([
                `${weapon.icon} __**${weapon.name}**__`,
                `**${language.get('knockout_hits')}**: ${hits} ${weapon.ammo}`,
                `**${language.get('knockout_damage')}**: ${damage}`,
                `**${language.get('knockout_torpor_instant')}**: ${hits} x ${weapon.torpor.instant} = ${hits * (weapon.torpor.instant)}`,
                `**${language.get('knockout_torpor_overtime')}**: ${hits} x ${weapon.torpor.overtime} = ${hits * (weapon.torpor.overtime)}`,
            ].join('\n'))
        }


        embed.setDescription(lines.join('\n\n').substr(0, 2048))
        embed.setTitle(sprintf(language.get('knockout_title'), level, creature.name));

        return message.reply(embed);
    }
};