const DBL = require("dblapi.js");
const {lists} = require(__dirname + '/../../config.json');
const dbl = new DBL(lists.topgg);

module.exports = {
    name: 'winner',
    description: 'Get a random user from the last 50 votes',
    usage: 'winner',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: true,
    hidden: true,
    premium: false,
    execute: async function (client, message, args, settings, language) {
        let users = await dbl.getVotes()
        users.slice(0, 50);
        let winner = users[Math.floor(Math.random() * users.length)];

        return message.reply(`The winner is ${winner.username}#${winner.discriminator} (${winner.id})`);
    }
};