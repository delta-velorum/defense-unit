const {MessageEmbed} = require('discord.js');

module.exports = {
    name: 'premium',
    description: 'Information about Delta Velorum premium',
    usage: 'premium',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            new MessageEmbed()
                .setDescription('[Click here for frequently asked questions or to subscribe to premium](https://ark.delta-velorum.com/premium)')
                .addField(
                    'Why?',
                    'Although most of our (Delta Velorum) services are free-to-use (and will likely always be free-to-use), someone has to make sure everything is up and running. Unfortunately that costs money, and since money does not grow on trees we have to pay for that. Over the years our projects have grown a lot, meaning that our upkeep costs go up too. If you enjoy using our services please consider subscribing or donating (subscribe and cancel immediately) to our Patreon page. \n\n**IMPORTANT**: You will be charged when you start your subscription __AND__ at the beginning of each month (unless you cancel your subscription, if you do so you will keep your premium status till the end of the month!).',
                )
                .addField(
                    'What are the features?',
                    [
                        '**Server watcher**: The server watcher will automatically update your server stats every 30 minutes and post them in a channel. Now allowing you to keep your server status on Discord!',
                    ]
                )
                .addField(
                    'What do you do with the money earned?',
                    'Most of the money will be spent on hosting, or saved up to pay for hosting later.'
                )
                .setColor(
                    'GREEN'
                )
        );
    }
};