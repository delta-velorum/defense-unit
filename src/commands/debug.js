const {MessageEmbed} = require('discord.js');
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);


module.exports = {
    name: 'debug',
    description: 'Displays data to help debugging the bot.',
    usage: ',debug',
    aliases: [],
    examples: [],
    guild: true,
    toggleable: false,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            new MessageEmbed()
                .setColor('GREEN')
                .addField('Shards utilized by this instance', client.shard.ids.join(', '))
                .addField('Bot language', '```' + JSON.stringify(language.get('pack')) + '```')
                .addField('Server ID', message.guild.id)
                .addField('Premium', settings.premium.toString())
                .addField('Permissions', [
                    `SEND_MESSAGES: ${message.guild.me.permissions.has("SEND_MESSAGES").toString()}`,
                    `MANAGE_MESSAGES: ${message.guild.me.permissions.has("MANAGE_MESSAGES").toString()}`,
                    `USE_EXTERNAL_EMOJIS: ${message.guild.me.permissions.has("USE_EXTERNAL_EMOJIS").toString()}`,
                    `EMBED_LINKS: ${message.guild.me.permissions.has("EMBED_LINKS").toString()}`,
                    `EMBED_LINKS: ${message.guild.me.permissions.has("EMBED_LINKS").toString()}`,
                ].join('\n'))
        )
    }
};