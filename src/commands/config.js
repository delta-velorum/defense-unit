const {MessageEmbed} = require('discord.js');

module.exports = {
    name: 'config',
    description: 'Configure settings for the bot.',
    usage: 'config',
    aliases: [],
    guild: false,
    toggleable: false,
    rate_limit: 0,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        return message.reply(
            new MessageEmbed()
                .setDescription('Server administrators can configure command channels, disable commands and more through [Defense Unit\'s dashboard](https://ark.delta-velorum.com).')
                .setColor(
                    'GREEN'
                )
        );
    }
};