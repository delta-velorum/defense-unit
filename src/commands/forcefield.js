const {MessageEmbed} = require('discord.js');
const Arguments = require('../util/Arguments');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

module.exports = {
    name: 'forcefield',
    description: 'Calculate the time before a forcefield runs out of element, use decimals for shards (0.01 = 1 shard)',
    usage: 'forcefied <element> [shards] [--radius=1] [--consumption=1]',
    example: [',ff 40', ',ff 0 30', ',ff 10 --radius=3'],
    aliases: ['ff'],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        if (!args[0]) {
            return message.reply(language.get('ff_must_specify_fuel_amount'))
        }

        if (isNaN(args[0])) {
            return message.reply(language.get('ff_amount_nan'))
        }

        let consumption = 1;
        let radius = 1;
        let fuel = parseFloat(args[0]);

        const arguments = new Arguments(args.join(' '));

        if (!isNaN(args[1])) {
            fuel += parseInt(args[1]) / 100;
        }

        if (fuel <= 0 || isNaN(fuel)) {
            return message.reply(language.get('ff_must_be_positive'))
        }

        if (fuel > 1e6) {
            return message.reply(sprintf(language.get('ff_max_number'), 1e6))
        }

        if (arguments.has('radius') && !isNaN(arguments.get('radius'))) {
            radius = parseInt(arguments.get('radius'))
        } else if (arguments.has('r') && !isNaN(arguments.get('r'))) {
            radius = parseInt(arguments.get('r'))
        }

        if (arguments.has('consumption') && !isNaN(arguments.get('consumption'))) {
            radius = parseInt(arguments.get('consumption'))
        } else if (arguments.has('c') && !isNaN(arguments.get('c'))) {
            radius = parseInt(arguments.get('c'))
        }

        if (consumption < 1 || consumption > 100) {
            consumption = 1;
        }

        if (radius < 1 || radius > 10000) {
            radius = 1;
        }

        let embed = new MessageEmbed();
        embed.setColor('GREEN');
        let numbers = fuel.toFixed(2).split('.');
        embed.setTitle(sprintf(language.get('ff_title'), numbers[0], Number(numbers[1])));

        const time = (Math.abs(fuel / radius) * consumption) * 3600; // abstract value of ((element / radius) * consumption) * 3600 = time in seconds
        const duration = moment.duration(time, 'seconds');

        embed.setDescription([
            sprintf(language.get('ff_runs_out_in'), duration.format('DD:HH:mm:ss', {
                trim: false
            })),
            '```yaml',
            `,ff ${Number(numbers[0])} ${Number(numbers[1])} --radius=${radius} --consumption=${consumption}`
            , '```']
        );

        return await message.reply(embed); // send the embed!
    }
};