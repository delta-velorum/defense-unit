const {MessageEmbed} = require('discord.js');
const sprintf = require('sprintf-js').sprintf;
const moment = require('moment');
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);


module.exports = {
    name: 'egen',
    description: 'Calculate the time before an electrical generator runs out of gasoline.',
    usage: 'tekgen <radius> <amount> [shards]',
    aliases: ['gasgen'],
    examples: [
        ',egen 12',
    ],
    guild: false,
    toggleable: true,
    rate_limit: 1,
    developer: false,
    execute: async function (client, message, args, settings, language) {
        const gas = Math.round(args[0]);

        if (!gas) {
            return client.commands.get('help').execute(client, message, ['egen'], settings, language);
        }

        const gasPerHour = 1;
        let duration = moment.duration((gas * gasPerHour) * 3600, 'seconds'); // gas * gasConsumedPerHour * 3600 = seconds
        let embed = new MessageEmbed();
        embed.setColor('GREEN');

        embed.setTitle(sprintf(language.get('electrical_generator_title'), gas));
        embed.setDescription([
            sprintf(language.get('electric_gen_runs_out_in'), duration.format('DD:HH:mm:ss', {
                trim: false
            })),
            '```yaml',
            `,egen ${gas}`,
            '```'
        ].join('\n'));

        return message.reply(embed);
    }
};