const fs = require('fs');
const items = require('./items.json');
const items2 = require('./andy');

let itemsnew = [];

for (let item of items) {
    let x = item;

    if (x.hasOwnProperty('prerequisite')) {
        if (typeof x.prerequisite === 'string') {
            x.prerequisites = x.prerequisite.split(',');
        } else {
            x.prerequisites = []
        }
    }

    if (!x.hasOwnProperty('defaultQuantity')) {
        x.default_quantity = 1;
    }

    if (x.hasOwnProperty('engramPoints')) {
        x.engram_points = x.engramPoints;
        delete x.defaultQuantity;
    }

    if (x.hasOwnProperty('craftedIn')) {
        x.crafted_in = x.craftedIn;
        delete x.craftedIn;
    }

    delete x.prerequisite;
    delete x.engramPoints;

    itemsnew.push(x);
}

for (let item of items2) {
    let x = item;

    if (x.hasOwnProperty('prerequisite')) {
        if (typeof x.prerequisite === 'string') {
            x.prerequisites = x.prerequisite.split(',');
        } else {
            x.prerequisites = []
        }
    }

    x.prerequisites = x.prerequisites.filter(x => x);

    if (!x.hasOwnProperty('defaultQuantity')) {
        x.default_quantity = 1;
    }

    if (x.hasOwnProperty('engramPoints')) {
        x.engram_points = x.engramPoints;
        delete x.defaultQuantity;
    }

    if (x.hasOwnProperty('craftedIn')) {
        x.crafted_in = x.craftedIn;
        delete x.craftedIn;
    }

    delete x.prerequisite;
    delete x.engramPoints;


    itemsnew.push(x);
}

fs.writeFileSync('items_new.json', JSON.stringify(itemsnew), 'utf-8');