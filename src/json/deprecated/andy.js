module.exports = [
    {
        "name": "Water Well",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/df/Water_Well_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901213133",
        "level": 15,
        "engramPoints": 10,
        "ingredients": [
            {
                "item": "Thatch",
                "quantity": 30
            },
            {
                "item": "Fiber",
                "quantity": 20
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 15
            },
            {
                "item": "Stone",
                "quantity": 50
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Flint",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Large Storage Box"
        ],
        "experience": 24
    },

    {
        "name": "Tree Sap Tap",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f3/Tree_Sap_Tap.png/revision/latest/scale-to-width-down/256?cb=20160706145227",
        "level": 36,
        "engramPoints": 18,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 100
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 40
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cementing Paste"
        ],
        "experience": 6
    },

    {
        "name": "Toilet",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/d5/Toilet.png/revision/latest/scale-to-width-down/256?cb=20170530165947",
        "level": 32,
        "engramPoints": 35,
        "ingredients": [
            {
                "item": "Stone",
                "quantity": 30
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 70
            },
            {
                "item": "Metal",
                "quantity": 180
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 200
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 40
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Stone Irrigation Pipe - Straight",
            "Stone Irrigation Pipe - Tap",
            "Water Reservoir"
        ],
        "experience": 0
    },

    {
        "name": "Oil Jar",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c1/Oil_Jar_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901205021",
        "level": 24,
        "engramPoints": 15,
        "ingredients": [
            {
                "item": "Propellant",
                "quantity": 3
            },
            {
                "item": "Gunpowder",
                "quantity": 3
            },
            {
                "item": "Clay",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Desert Cloth Pants",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c0/Desert_Cloth_Pants_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901235312",
        "level": 28,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Hide",
                "quantity": 80
            },
            {
                "item": "Fiber",
                "quantity": 15
            },
            {
                "item": "Silk",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cloth Pants"
        ],
        "experience": 5.6
    },

    {
        "name": "Desert Goggles and Hat",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/99/Desert_Goggles_and_Hat_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901234320",
        "level": 28,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Hide",
                "quantity": 10
            },
            {
                "item": "Fiber",
                "quantity": 6
            },
            {
                "item": "Silica Pearls or Silicate",
                "quantity": 3
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 10
            },
            {
                "item": "Silk",
                "quantity": 3
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cloth Hat"
        ],
        "experience": 3.2
    },

    {
        "name": "Desert Cloth Gloves",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/32/Desert_Cloth_Gloves_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901234813",
        "level": 28,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Hide",
                "quantity": 10
            },
            {
                "item": "Fiber",
                "quantity": 4
            },
            {
                "item": "Silk",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cloth Gloves"
        ],
        "experience": 2
    },

    {
        "name": "Desert Cloth Shirt",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/41/Desert_Cloth_Shirt_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901235202",
        "level": 28,
        "engramPoints": 4,
        "ingredients": [
            {
                "item": "Hide",
                "quantity": 20
            },
            {
                "item": "Fiber",
                "quantity": 8
            },
            {
                "item": "Silk",
                "quantity": 12
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cloth Shirt"
        ],
        "experience": 6
    },

    {
        "name": "Desert Cloth Boots",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/29/Desert_Cloth_Boots_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901235418",
        "level": 28,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Hide",
                "quantity": 12
            },
            {
                "item": "Fiber",
                "quantity": 5
            },
            {
                "item": "Silk",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Cloth Boots"
        ],
        "experience": 2.8
    },

    {
        "name": "Chainsaw",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/5/5f/Chainsaw_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160902000228",
        "level": 55,
        "engramPoints": 21,
        "ingredients": [
            {
                "item": "Polymer, Organic Polymer, or Corrupted Nodule",
                "quantity": 75
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 50
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 50
            },
            {
                "item": "Electronics",
                "quantity": 25
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Shotgun"
        ],
        "experience": 120
    },

    {
        "name": "Flamethrower",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/47/Flamethrower_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901234619",
        "level": 65,
        "engramPoints": 25,
        "ingredients": [
            {
                "item": "Polymer, Organic Polymer, or Corrupted Nodule",
                "quantity": 75
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 50
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 35
            },
            {
                "item": "Sulfur",
                "quantity": 10
            },
            {
                "item": "Electronics",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Assault Rifle"
        ],
        "experience": 140
    },

    {
        "name": "Mirror",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/4d/Mirror_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901233346",
        "level": 32,
        "engramPoints": 25,
        "ingredients": [
            {
                "item": "Sand",
                "quantity": 100
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 7
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 100
            },
            {
                "item": "Obsidian",
                "quantity": 70
            },
            {
                "item": "Silica Pearls or Silicate",
                "quantity": 100
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 50
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Metal Wall Sign"
        ],
        "experience": 4
    },

    {
        "name": "Tent",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/d6/Tent_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/600?cb=20170117190945",
        "level": 10,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Thatch",
                "quantity": 25
            },
            {
                "item": "Fiber",
                "quantity": 10
            },
            {
                "item": "Hide",
                "quantity": 50
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            },
            {
                "item": "Silk",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Hide Sleeping Bag"
        ],
        "experience": 10
    },

    {
        "name": "Cruise Missile",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/1/13/Cruise_Missile_%28Genesis_Part_1%29.png/revision/latest/scale-to-width-down/256?cb=20200226081922",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Polymer, Organic Polymer, or Corrupted Nodule",
                "quantity": 212
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 256
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 160
            },
            {
                "item": "Element",
                "quantity": 14
            },
            {
                "item": "Black Pearl",
                "quantity": 26
            },
            {
                "item": "Electronics",
                "quantity": 128
            }
        ],
        "craftedIn": [
            "Tek Replicator"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 972
    },

    {
        "name": "Pressure Plate",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/a2/Pressure_Plate_%28Genesis_Part_1%29.png/revision/latest/scale-to-width-down/256?cb=20200226081111",
        "level": 12,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 32
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 5
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 130
            },
            {
                "item": "Fiber",
                "quantity": 68
            },
            {
                "item": "Thatch",
                "quantity": 170
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 200
    },

    {
        "name": "Water Reservoir",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/2c/Water_Reservoir.png/revision/latest/scale-to-width-down/256?cb=20150615134755",
        "level": 17,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Stone",
                "quantity": 30
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Cementing Paste"
        ],
        "experience": 6
    },

    {
        "name": "War Map",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/7/7a/War_Map.png/revision/latest?cb=20151021221638",
        "level": 51,
        "engramPoints": 15,
        "ingredients": [
            {
                "item": "Fiber",
                "quantity": 4
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 2
            },
            {
                "item": "Hide",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 6.4
    },

    {
        "name": "Flamethrower",
        "image": "",
        "level": 65,
        "engramPoints": 25,
        "ingredients": [
            {
                "item": "Polymer, Organic Polymer, or Corrupted Nodule",
                "quantity": 75
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 50
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 35
            },
            {
                "item": "Sulfur",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Assault Rifle"
        ],
        "experience": 140
    },

    {
        "name": "Wall Torch",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c8/Wall_Torch.png/revision/latest/scale-to-width-down/256?cb=20151123214033",
        "level": 21,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Thatch",
                "quantity": 4
            },
            {
                "item": "Flint",
                "quantity": 1
            },
            {
                "item": "Stone",
                "quantity": 1
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 2
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 1
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Standing Torch"
        ],
        "experience": 4.8
    },

    {
        "name": "Trophy Wall-Mount",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f3/Trophy_Wall-Mount.png/revision/latest/scale-to-width-down/256?cb=20151107061841",
        "level": 22,
        "engramPoints": 10,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 200
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 80
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Wooden Wall Sign"
        ],
        "experience": 14.8
    },

    {
        "name": "Training Dummy",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/49/Training_Dummy.png/revision/latest/scale-to-width-down/256?cb=20160611103602",
        "level": 9,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 80
            },
            {
                "item": "Fiber",
                "quantity": 500
            },
            {
                "item": "Hide",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 500
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 4
    },

    {
        "name": "Rope Ladder",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e1/Rope_Ladder.png/revision/latest?cb=20160706145134",
        "level": 16,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            },
            {
                "item": "Thatch",
                "quantity": 140
            },
            {
                "item": "Fiber",
                "quantity": 180
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 1.992
    },

    {
        "name": "Painting Canvas",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/a1/Painting_Canvas.png/revision/latest/scale-to-width-down/256?cb=20150925012800",
        "level": 21,
        "engramPoints": 2,
        "ingredients": [
            {
                "item": "Fiber",
                "quantity": 4
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 2
            },
            {
                "item": "Hide",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Wooden Wall Sign"
        ],
        "experience": 6.4
    },

    {
        "name": "Metal Water Reservoir",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/cc/Metal_Water_Reservoir.png/revision/latest/scale-to-width-down/256?cb=20150707012523",
        "level": 41,
        "engramPoints": 20,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 75
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 25
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Water Reservoir"
        ],
        "experience": 6
    },

    {
        "name": "Adobe Ceiling",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/6/64/Adobe_Ceiling_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901192621",
        "level": 15,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 60
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 30
            },
            {
                "item": "Thatch",
                "quantity": 15
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Ceiling"
        ],
        "experience": 26
    },

    {
        "name": "Adobe Dinosaur Gate",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/1/1e/Adobe_Dinosaur_Gate_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901201809",
        "level": 30,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 60
            },
            {
                "item": "Clay",
                "quantity": 50
            },
            {
                "item": "Thatch",
                "quantity": 15
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Door"
        ],
        "experience": 56
    },

    {
        "name": "Adobe Dinosaur Gateway",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/21/Adobe_Dinosaur_Gateway_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901202015",
        "level": 30,
        "engramPoints": 12,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 280
            },
            {
                "item": "Clay",
                "quantity": 80
            },
            {
                "item": "Thatch",
                "quantity": 70
            },
            {
                "item": "Fiber",
                "quantity": 50
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Doorframe"
        ],
        "experience": 52.8
    },

    {
        "name": "Adobe Door",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/96/Adobe_Door_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901191410",
        "level": 15,
        "engramPoints": 4,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 20
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 7
            },
            {
                "item": "Fiber",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Door",
            "Adobe Doorframe"
        ],
        "experience": 10
    },

    {
        "name": "Adobe Doorframe",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/31/Adobe_Doorframe_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901190332",
        "level": 15,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 30
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            },
            {
                "item": "Thatch",
                "quantity": 8
            },
            {
                "item": "Fiber",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Doorframe",
            "Adobe Wall"
        ],
        "experience": 12.4
    },

    {
        "name": "Adobe Double Door",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/7/77/Adobe_Double_Door_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20190306004419",
        "level": 22,
        "engramPoints": 4,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 40
            },
            {
                "item": "Fiber",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 14
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Door"
        ],
        "experience": 10
    },

    {
        "name": "Adobe Double Doorframe",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/3a/Adobe_Double_Doorframe_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190306004451",
        "level": 22,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 30
            },
            {
                "item": "Fiber",
                "quantity": 8
            },
            {
                "item": "Thatch",
                "quantity": 8
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Doorframe"
        ],
        "experience": 12.4
    },

    {
        "name": "Adobe Fence Foundation",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e7/Adobe_Fence_Foundation_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901201903",
        "level": 15,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 10
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 5
            },
            {
                "item": "Thatch",
                "quantity": 3
            },
            {
                "item": "Fiber",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Foundation"
        ],
        "experience": 8.76
    },

    {
        "name": "Adobe Fence Support",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f7/Adobe_Fence_Support_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190306004619",
        "level": 15,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 10
            },
            {
                "item": "Fiber",
                "quantity": 5
            },
            {
                "item": "Thatch",
                "quantity": 3
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Foundation"
        ],
        "experience": 8.76
    },

    {
        "name": "Adobe Foundation",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/5/55/Adobe_Foundation_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901202214",
        "level": 15,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 80
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 40
            },
            {
                "item": "Thatch",
                "quantity": 20
            },
            {
                "item": "Fiber",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Foundation"
        ],
        "experience": 41.2
    },

    {
        "name": "Adobe Hatchframe",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/3f/Adobe_Hatchframe_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901192000",
        "level": 16,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 50
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 25
            },
            {
                "item": "Thatch",
                "quantity": 12
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Ceiling"
        ],
        "experience": 17.6
    },

    {
        "name": "Adobe Ladder",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e7/Adobe_Ladder_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901195045",
        "level": 16,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 4
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 5
            },
            {
                "item": "Thatch",
                "quantity": 7
            },
            {
                "item": "Fiber",
                "quantity": 3
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 6
    },

    {
        "name": "Adobe Pillar",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/a8/Adobe_Pillar_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901201243",
        "level": 17,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 40
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            },
            {
                "item": "Thatch",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Foundation"
        ],
        "experience": 15.2
    },

    {
        "name": "Adobe Railing",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/bf/Adobe_Railing_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901201145",
        "level": 17,
        "engramPoints": 2,
        "ingredients": [
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            },
            {
                "item": "Clay",
                "quantity": 5
            },
            {
                "item": "Thatch",
                "quantity": 5
            },
            {
                "item": "Fiber",
                "quantity": 3
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Wall"
        ],
        "experience": 4.4
    },

    {
        "name": "Adobe Ramp",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c0/Adobe_Ramp_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901201022",
        "level": 16,
        "engramPoints": 3,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 60
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 30
            },
            {
                "item": "Thatch",
                "quantity": 15
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Ceiling"
        ],
        "experience": 31.6
    },

    {
        "name": "Adobe Staircase",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/eb/Adobe_Staircase_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160903173944",
        "level": 17,
        "engramPoints": 15,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 200
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 100
            },
            {
                "item": "Thatch",
                "quantity": 50
            },
            {
                "item": "Fiber",
                "quantity": 40
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Wall"
        ],
        "experience": 11.6
    },

    {
        "name": "Adobe Stairs",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/28/Adobe_Stairs_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190306012709",
        "level": 16,
        "engramPoints": 3,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 60
            },
            {
                "item": "Fiber",
                "quantity": 15
            },
            {
                "item": "Thatch",
                "quantity": 10
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 30
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 31.6
    },

    {
        "name": "Adobe Trapdoor",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/46/Adobe_Trapdoor_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901192508",
        "level": 16,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 20
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 7
            },
            {
                "item": "Fiber",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 13.6
    },

    {
        "name": "Adobe Triangle Ceiling",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/24/Adobe_Triangle_Ceiling_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190416201131",
        "level": 15,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 30
            },
            {
                "item": "Fiber",
                "quantity": 5
            },
            {
                "item": "Thatch",
                "quantity": 7
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Ceiling"
        ],
        "experience": 12
    },

    {
        "name": "Adobe Triangle Foundation",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/da/Adobe_Triangle_Foundation_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190416200633",
        "level": 15,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 40
            },
            {
                "item": "Fiber",
                "quantity": 8
            },
            {
                "item": "Thatch",
                "quantity": 10
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Foundation"
        ],
        "experience": 20
    },

    {
        "name": "Adobe Triangle Roof",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/5/5f/Adobe_Triangle_Roof_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190416201655",
        "level": 18,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 40
            },
            {
                "item": "Fiber",
                "quantity": 8
            },
            {
                "item": "Thatch",
                "quantity": 10
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Sloped Thatch Roof",
            "Adobe Triangle Ceiling"
        ],
        "experience": 11.6
    },

    {
        "name": "Adobe Wall",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e4/Adobe_Wall_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901194911",
        "level": 15,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 40
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            },
            {
                "item": "Thatch",
                "quantity": 10
            },
            {
                "item": "Fiber",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Thatch Wall"
        ],
        "experience": 11.6
    },

    {
        "name": "Adobe Window",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/6/6b/Adobe_Window_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901194749",
        "level": 20,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 8
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 4
            },
            {
                "item": "Thatch",
                "quantity": 2
            },
            {
                "item": "Fiber",
                "quantity": 4
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Windowframe"
        ],
        "experience": 5.6
    },

    {
        "name": "Adobe Windowframe",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/8/86/Adobe_Windowframe_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901193246",
        "level": 20,
        "engramPoints": 8,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 36
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 15
            },
            {
                "item": "Thatch",
                "quantity": 9
            },
            {
                "item": "Fiber",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Wall"
        ],
        "experience": 14
    },

    {
        "name": "Behemoth Adobe Dinosaur Gate",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/bf/Behemoth_Adobe_Dinosaur_Gate_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901201647",
        "level": 38,
        "engramPoints": 15,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 450
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 450
            },
            {
                "item": "Thatch",
                "quantity": 450
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Adobe Dinosaur Gateway"
        ],
        "experience": 64
    },

    {
        "name": "Behemoth Adobe Dinosaur Gateway",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/aa/Behemoth_Adobe_Dinosaur_Gateway_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901201434",
        "level": 38,
        "engramPoints": 18,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 900
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 700
            },
            {
                "item": "Thatch",
                "quantity": 800
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Adobe Dinosaur Gate"
        ],
        "experience": 80
    },

    {
        "name": "Giant Adobe Hatchframe",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/1/17/Giant_Adobe_Hatchframe_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901191834",
        "level": 34,
        "engramPoints": 15,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 70
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 20
            },
            {
                "item": "Thatch",
                "quantity": 10
            },
            {
                "item": "Fiber",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Adobe Hatchframe"
        ],
        "experience": 28.8
    },

    {
        "name": "Giant Adobe Trapdoor",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/fe/Giant_Adobe_Trapdoor_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20160901192118",
        "level": 34,
        "engramPoints": 12,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 100
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 45
            },
            {
                "item": "Thatch",
                "quantity": 20
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Giant Adobe Hatchframe"
        ],
        "experience": 27.6
    },

    {
        "name": "Large Adobe Wall",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/1/10/Large_Adobe_Wall_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/256?cb=20190416202055",
        "level": 19,
        "engramPoints": 9,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 160
            },
            {
                "item": "Fiber",
                "quantity": 32
            },
            {
                "item": "Thatch",
                "quantity": 40
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 80
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Adobe Wall"
        ],
        "experience": 48
    },

    {
        "name": "Sloped Adobe Roof",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/dd/Sloped_Adobe_Roof_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901191637",
        "level": 18,
        "engramPoints": 5,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 60
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 30
            },
            {
                "item": "Thatch",
                "quantity": 15
            },
            {
                "item": "Fiber",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Sloped Thatch Roof",
            "Adobe Ceiling"
        ],
        "experience": 26.8
    },

    {
        "name": "Sloped Adobe Wall Left",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/ff/Sloped_Adobe_Wall_Left_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901195338",
        "level": 18,
        "engramPoints": 3,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 20
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 5
            },
            {
                "item": "Fiber",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Sloped Thatch Wall Left",
            "Adobe Wall"
        ],
        "experience": 7.76
    },

    {
        "name": "Sloped Adobe Wall Right",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/2/2f/Sloped_Adobe_Wall_Right_%28Scorched_Earth%29.png/revision/latest/scale-to-width-down/512?cb=20160901195225",
        "level": 18,
        "engramPoints": 3,
        "ingredients": [
            {
                "item": "Clay",
                "quantity": 20
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 10
            },
            {
                "item": "Thatch",
                "quantity": 5
            },
            {
                "item": "Fiber",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            "Sloped Thatch Wall Right",
            "Adobe Wall"
        ],
        "experience": 7.76
    },

    {
        "name": "Poison Grenade",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/05/Poison_Grenade.png/revision/latest/scale-to-width-down/256?cb=20151003050827",
        "level": 40,
        "engramPoints": 18,
        "ingredients": [
            {
                "item": "Narcotic",
                "quantity": 10
            },
            {
                "item": "Charcoal",
                "quantity": 14
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 5
            },
            {
                "item": "Sparkpowder",
                "quantity": 18
            },
            {
                "item": "Gunpowder",
                "quantity": 12
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 5
            },
            {
                "item": "Fiber",
                "quantity": 20
            },
            {
                "item": "Flint",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Smoke Grenade",
            "Tripwire Narcotic Trap"
        ],
        "experience": 33
    },

    {
        "name": "Electric Prod",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/48/Electric_Prod.png/revision/latest/scale-to-width-down/256?cb=20160131044420",
        "level": 95,
        "engramPoints": 60,
        "ingredients": [
            {
                "item": "Polymer, Organic Polymer, or Corrupted Nodule",
                "quantity": 120
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 130
            },
            {
                "item": "Electronics",
                "quantity": 120
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 70
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 120
            },
            {
                "item": "AnglerGel",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 26.4
    },

    {
        "name": "Flashlight Attachment",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/a3/Flashlight_Attachment.png/revision/latest/scale-to-width-down/256?cb=20150615092604",
        "level": 52,
        "engramPoints": 18,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 40
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 40
            },
            {
                "item": "Electronics",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 304
    },

    {
        "name": "Holo-Scope Attachment",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/91/Holo-Scope_Attachment.png/revision/latest/scale-to-width-down/256?cb=20150615180016",
        "level": 77,
        "engramPoints": 24,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 40
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 40
            },
            {
                "item": "Electronics",
                "quantity": 30
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 400
    },

    {
        "name": "Laser Attachment",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/b3/Laser_Attachment.png/revision/latest/scale-to-width-down/256?cb=20150615103328",
        "level": 59,
        "engramPoints": 24,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 50
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 60
            },
            {
                "item": "Electronics",
                "quantity": 40
            }
        ],
        "craftedIn": [
            "Fabricator",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Flashlight Attachment"
        ],
        "experience": 544
    },

    {
        "name": "Scope Attachment",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/b3/Scope_Attachment.png/revision/latest/scale-to-width-down/256?cb=20150927205833",
        "level": 35,
        "engramPoints": 13,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 40
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 20
            },
            {
                "item": "Stone",
                "quantity": 5
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Spyglass"
        ],
        "experience": 196
    },

    {
        "name": "Silencer Attachment",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/39/Silencer_Attachment.png/revision/latest/scale-to-width-down/256?cb=20150615092813",
        "level": 41,
        "engramPoints": 13,
        "ingredients": [
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 50
            },
            {
                "item": "Oil",
                "quantity": 5
            },
            {
                "item": "Chitin, Keratin, or Shell Fragment",
                "quantity": 20
            },
            {
                "item": "Hide",
                "quantity": 10
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 196
    },

    {
        "name": "Tripwire Alarm Trap",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/d5/Tripwire_Alarm_Trap.png/revision/latest/scale-to-width-down/256?cb=20150722134113",
        "level": 22,
        "engramPoints": 7,
        "ingredients": [
            {
                "item": "Metal",
                "quantity": 3
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 5
            },
            {
                "item": "Fiber",
                "quantity": 30
            },
            {
                "item": "Hide",
                "quantity": 6
            },
            {
                "item": "Oil",
                "quantity": 2
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 5.4
    },

    {
        "name": "Tripwire Narcotic Trap",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/ab/Tripwire_Narcotic_Trap.png/revision/latest/scale-to-width-down/256?cb=20150722134018",
        "level": 29,
        "engramPoints": 9,
        "ingredients": [
            {
                "item": "Narcotic",
                "quantity": 15
            },
            {
                "item": "Cementing Paste or Achatina Paste",
                "quantity": 3
            },
            {
                "item": "Wood or Fungal Wood",
                "quantity": 4
            },
            {
                "item": "Fiber",
                "quantity": 35
            },
            {
                "item": "Hide",
                "quantity": 6
            },
            {
                "item": "Crystal or Primal Crystal",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 7.8
    },

    {
        "name": "Bear Trap",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/92/Bear_Trap.png/revision/latest/scale-to-width-down/256?cb=20150806121724",
        "level": 28,
        "engramPoints": 9,
        "ingredients": [
            {
                "item": "Fiber",
                "quantity": 5
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 3
            },
            {
                "item": "Hide",
                "quantity": 6
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Smithy"
        ],
        "experience": 32
    },

    {
        "name": "Large Bear Trap",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/7/7c/Large_Bear_Trap.png/revision/latest/scale-to-width-down/256?cb=20150806121650",
        "level": 31,
        "engramPoints": 12,
        "ingredients": [
            {
                "item": "Fiber",
                "quantity": 10
            },
            {
                "item": "Metal Ingot or Scrap Metal Ingot",
                "quantity": 6
            },
            {
                "item": "Hide",
                "quantity": 15
            }
        ],
        "craftedIn": [
            "Smithy",
            "Argentavis Saddle",
            "Castoroides Saddle",
            "Thorny Dragon Saddle",
            "Tek Replicator"
        ],
        "prerequisite": [
            "Bear Trap"
        ],
        "experience": 32
    },

    {
        "name": "Bug Repellant",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/47/Bug_Repellant.png/revision/latest/scale-to-width-down/256?cb=20151003050745",
        "level": 16,
        "engramPoints": 12,
        "ingredients": [
            {
                "item": "Pelt, Hair, or Wool",
                "quantity": 6
            },
            {
                "item": "Narcotic",
                "quantity": 2
            },
            {
                "item": "Citronal",
                "quantity": 4
            },
            {
                "item": "Rockarrot",
                "quantity": 4
            }
        ],
        "chemistry_bench": [
            {
                "item": "Pelt, Hair, or Wool",
                "quantity": 24
            },
            {
                "item": "Narcotic",
                "quantity": 8
            },
            {
                "item": "Citronal",
                "quantity": 16
            },
            {
                "item": "Rockarrot",
                "quantity": 16
            }
        ],
        "craftedIn": [
            "Mortar and Pestle",
            "Chemistry Bench",
            "Equus Saddle"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 4
    },

    {
        "name": "Lesser Antidote",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/8/84/Lesser_Antidote.png/revision/latest/scale-to-width-down/256?cb=20160606191728",
        "level": 18,
        "engramPoints": 16,
        "ingredients": [
            {
                "item": "Rare Flower",
                "quantity": 10
            },
            {
                "item": "Rare Mushroom",
                "quantity": 10
            },
            {
                "item": "Leech Blood or Horns",
                "quantity": 3
            },
            {
                "item": "Narcotic",
                "quantity": 1
            }
        ],
        "chemistry_bench": [
            {
                "item": "Rare Flower",
                "quantity": 40
            },
            {
                "item": "Rare Mushroom",
                "quantity": 40
            },
            {
                "item": "Leech Blood or Horns",
                "quantity": 12
            },
            {
                "item": "Narcotic",
                "quantity": 4
            }
        ],
        "craftedIn": [
            "Mortar and Pestle",
            "Chemistry Bench",
            "Equus Saddle"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 4
    },

    {
        "name": "Re-Fertilizer",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/1/17/Re-Fertilizer.png/revision/latest/scale-to-width-down/256?cb=20150815204612",
        "level": 38,
        "engramPoints": 20,
        "ingredients": [
            {
                "item": "Rare Mushroom",
                "quantity": 1
            },
            {
                "item": "Rare Flower",
                "quantity": 1
            },
            {
                "item": "Sparkpowder",
                "quantity": 4
            },
            {
                "item": "Fertilizer",
                "quantity": 1
            },
            {
                "item": "Oil",
                "quantity": 3
            }
        ],
        "craftedIn": [
            "Inventory"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 4
    },

    {
        "name": "Stimulant",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e2/Stimulant.png/revision/latest/scale-to-width-down/256?cb=20150615115134",
        "level": 11,
        "engramPoints": 6,
        "ingredients": [
            {
                "item": "Stimberry",
                "quantity": 5
            },
            {
                "item": "Sparkpowder",
                "quantity": 2
            }
        ],
        "chemistry_bench": [
            {
                "item": "Stimberry",
                "quantity": 20
            },
            {
                "item": "Sparkpowder",
                "quantity": 8
            }
        ],
        "craftedIn": [
            "Mortar and Pestle",
            "Chemistry Bench",
            "Equus Saddle"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0.2
    },

    {
        "name": "Black Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f6/Black_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005232105",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Narcoberry",
                "quantity": 15
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Blue Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f2/Blue_Coloring.png/revision/latest/scale-to-width-down/256?cb=20150616132615",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Azulberry",
                "quantity": 15
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Brick Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/ff/Brick_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005232749",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 12
            },
            {
                "item": "Narcoberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Brown Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/02/Brown_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005233117",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Tintoberry",
                "quantity": 9
            },
            {
                "item": "Amarberry",
                "quantity": 6
            },
            {
                "item": "Azulberry",
                "quantity": 3
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Cantaloupe Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/5/50/Cantaloupe_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005233317",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 7
            },
            {
                "item": "Amarberry",
                "quantity": 7
            },
            {
                "item": "Stimberry",
                "quantity": 4
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Cyan Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/b6/Cyan_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005234003",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Azulberry",
                "quantity": 12
            },
            {
                "item": "Amarberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Forest Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/b9/Forest_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005223937",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Amarberry",
                "quantity": 7
            },
            {
                "item": "Azulberry",
                "quantity": 7
            },
            {
                "item": "Narcoberry",
                "quantity": 4
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Green Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c2/Green_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005234251",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Amarberry",
                "quantity": 9
            },
            {
                "item": "Azulberry",
                "quantity": 9
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Magenta Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/e/e2/Magenta_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005222635",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Azulberry",
                "quantity": 9
            },
            {
                "item": "Tintoberry",
                "quantity": 9
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Mud Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/4a/Mud_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005234652",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 7
            },
            {
                "item": "Amarberry",
                "quantity": 4
            },
            {
                "item": "Azulberry",
                "quantity": 1
            },
            {
                "item": "Narcoberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Navy Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/8/80/Navy_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005234844",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Azulberry",
                "quantity": 12
            },
            {
                "item": "Narcoberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Olive Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/b/b5/Olive_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151005235030",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Amarberry",
                "quantity": 12
            },
            {
                "item": "Narcoberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Orange Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/7/74/Orange_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006000418",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Tintoberry",
                "quantity": 9
            },
            {
                "item": "Amarberry",
                "quantity": 9
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Parchement Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/0b/Parchement_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006000726",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Amarberry",
                "quantity": 12
            },
            {
                "item": "Stimberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Pink Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/3/30/Pink_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006000924",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 12
            },
            {
                "item": "Stimberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Purple Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/0d/Purple_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006001219",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Tintoberry",
                "quantity": 9
            },
            {
                "item": "Azulberry",
                "quantity": 9
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Red Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/d/d3/Red_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006001357",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Tintoberry",
                "quantity": 15
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Royalty Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f6/Royalty_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006001554",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Azulberry",
                "quantity": 7
            },
            {
                "item": "Tintoberry",
                "quantity": 7
            },
            {
                "item": "Narcoberry",
                "quantity": 4
            },
            {
                "item": "Water",
                "quantity": 1
            },
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Silver Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/8/88/Silver_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006001736",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Narcoberry",
                "quantity": 6
            },
            {
                "item": "Stimberry",
                "quantity": 12
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Sky Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/f/f7/Sky_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006001858",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Azulberry",
                "quantity": 12
            },
            {
                "item": "Stimberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Slate Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/4/4b/Slate_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006002024",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Sparkpowder",
                "quantity": 1
            },
            {
                "item": "Narcoberry",
                "quantity": 12
            },
            {
                "item": "Stimberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Tan Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/a/ab/Tan_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006002154",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 7
            },
            {
                "item": "Amarberry",
                "quantity": 4
            },
            {
                "item": "Azulberry",
                "quantity": 1
            },
            {
                "item": "Stimberry",
                "quantity": 6
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Tangerine Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/9/9e/Tangerine_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006002322",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Gunpowder",
                "quantity": 1
            },
            {
                "item": "Tintoberry",
                "quantity": 7
            },
            {
                "item": "Amarberry",
                "quantity": 7
            },
            {
                "item": "Narcoberry",
                "quantity": 4
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "White Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/7/75/White_Coloring.png/revision/latest/scale-to-width-down/256?cb=20150616132405",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Stimberry",
                "quantity": 15
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },

    {
        "name": "Yellow Coloring",
        "image": "https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/0c/Yellow_Coloring.png/revision/latest/scale-to-width-down/256?cb=20151006002617",
        "level": 0,
        "engramPoints": 0,
        "ingredients": [
            {
                "item": "Charcoal",
                "quantity": 2
            },
            {
                "item": "Amarberry",
                "quantity": 15
            },
            {
                "item": "Water",
                "quantity": 1
            }
        ],
        "craftedIn": [
            "Cooking Pot",
            "Industrial Cooker"
        ],
        "prerequisite": [
            ""
        ],
        "experience": 0
    },
]