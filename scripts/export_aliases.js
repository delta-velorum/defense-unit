const fs = require('fs');

const items = require('../src/json/items.json');

let result = [];

for (const item of items) {
    result.push({
        name: item.name,
        aliases: item.hasOwnProperty('aliases') ? item.aliases : []
    })
}

fs.writeFileSync('aliases.json', JSON.stringify(result), 'utf-8')