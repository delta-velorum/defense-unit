const fs = require('fs');

const items = require('../src/json/items.json');

let map = new Set();

for (const item of items) {
    for (const x of item.ingredients) {
        if (!map.has(x.item)) {
            map.add(x.item);
        }
    }
}

fs.writeFileSync('material_emojis.json', JSON.stringify([...map]), 'utf-8')