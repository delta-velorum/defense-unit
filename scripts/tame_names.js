const {species} = require(__dirname + '/../src/json/creature_stats');
const creatures = require(__dirname + '/../src/json/creatures');
const fs = require('fs');

let x = [];

for (const creature of species) {
    let names = {};

    if (creatures.some(y => y.name === creature.name)) {
        names = creatures.filter(y => y.name === creature.name)[0];
    }
    x.push({
        name: creature.name,
        ...names
    });
}

fs.writeFileSync(__dirname + '/tame_names.json', JSON.stringify(x), 'UTF-8');