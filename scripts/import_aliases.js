const fs = require('fs');

const items = require('../src/json/items.json');
const aliases = require('./aliases.json');

let result = [];

for (let item of items) {

    let x = aliases.filter(y => y.name === item.name);

    if (x[0]) {
        item.aliases = x[0].aliases;
    }

    result.push(item)
}

fs.writeFileSync('items2.json', JSON.stringify(result), 'utf-8')