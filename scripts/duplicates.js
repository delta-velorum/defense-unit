const {species} = require(__dirname + '/../src/json/creature_stats');

let x = {};

for (const creature of species) {
    if (x.hasOwnProperty(creature.name)) {
        x[creature.name] = x[creature.name] + 1;
    } else {
        x[creature.name] = 1;
    }
}

console.log(Object.entries(x).filter(([key, value]) => value > 1))