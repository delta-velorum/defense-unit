
exports.up = function(knex) {
    return knex.schema.createTable('trades', function (table) {
        table.bigIncrements('id');
        table.string('user_id', 64).notNullable();
        table.string('guild_id', 64).notNullable();
        table.string('message_id', 64).nullable();
        table.string('have', 1024).notNullable();
        table.string('want', 1024).notNullable();
        table.string('status', 32).defaultTo('open');
        table.timestamps(true, true);

        table.index(['user_id', 'guild_id']);
        table.index(['guild_id']);

        table.foreign('user_id').references('id').inTable('users');
        table.foreign('guild_id').references('id').inTable('guilds');

        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('trades');
};
