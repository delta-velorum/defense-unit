
exports.up = function(knex) {
    return knex.schema.createTable('actions', function (table) {
       table.bigIncrements();
       table.string('user_id', 64);
       table.text('info');
       table.string('type', 128);

       table.index('user_id');
       table.index('type');
       table.index(['user_id', 'type']);

        table.timestamps(true, true);
        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('actions');
};
