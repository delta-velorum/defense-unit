exports.up = function (knex) {
    return knex.schema.createTable('users', function (table) {
        table.string('id', 64).primary();
        table.boolean('is_premium').defaultTo(false);
        table.boolean('is_blacklisted').defaultTo(false);
        table.boolean('is_sharing_logs').defaultTo(false);
        table.text('notes').nullable();
        table.timestamps(true, true);

        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('users');
};
