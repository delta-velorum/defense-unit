exports.up = function (knex) {
    return knex.schema.createTable('logs', function (table) {
        table.bigIncrements('id').primary();
        table.string('user_id', 64);
        table.text('content');
        table.string('title', 128).nullable();

        table.foreign('user_id').references('id').inTable('users').onDelete('cascade');
        table.index('id');
        table.index('user_id');
        table.index(['user_id', 'id']);

        table.timestamps(true, true);
        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('logs');
};
