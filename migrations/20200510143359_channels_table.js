
exports.up = function(knex) {
    return knex.schema.createTable('channels', function (table) {
        table.string('id', 64).notNullable();
        table.string('guild_id', 64).notNullable();
        table.string('type').notNullable();
        table.timestamps(true, true);

        table.foreign('guild_id').references('id').inTable('guilds');

        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('channels')
};
