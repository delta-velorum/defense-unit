exports.up = function (knex) {
    return knex.schema.createTable('guilds', function (table) {
        table.string('id', 64).primary();
        table.integer('default_level').unsigned().defaultTo(150);
        table.integer('default_multiplier').unsigned().defaultTo(1);
        table.integer('default_consumption').unsigned().defaultTo(1);
        table.boolean('tickets_enabled').defaultTo(false);
        table.boolean('trading_enabled').defaultTo(false);
        table.timestamps(true, true);

        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('guilds')
};
