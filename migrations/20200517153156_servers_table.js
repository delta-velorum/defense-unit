exports.up = function (knex) {
    return knex.schema.createTable('servers', function (table) {
        table.bigIncrements('id');
        table.string('name', 256);
        table.string('platform', 64);
        table.string('server_id', 256);
        table.string('guild_id', 64);

        table.foreign('guild_id').references('id').inTable('guilds');


        table.timestamps(true, true);
        table.charset('utf8mb4');
        table.collate('utf8mb4_unicode_ci');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('servers');
};
